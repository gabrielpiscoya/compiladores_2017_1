/*
  Coloque aqui o identificador do grupo e dos seus membros
*/
%{
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "../include/cc_dict.h"
#include "../include/main.h"
#include <string.h>
//#include "../y.tab.h"		// utilizar yacc -d -v parser.y para gerar os arquivos
int lineNumber = 1;	// Contador de linha do arquivo 
int i;

comp_dict_t *SymbolTable;
int ret_esperado;


void* insertSymbolOnTable(char* token, int line, int type){



	char *tokenbuffer;
	char buffernum[2];
//--------------------------------------------------------------------------
	tokenbuffer = (char*)malloc(strlen(token)+1);
//	tokenbuffer = (char*)malloc((strlen(token)+2));
	strcpy(tokenbuffer, token);

	//tokenbuffer[strlen(token)+1] = '\0';

	//sprintf(buffernum, "%d", type);
	//tokenbuffer[strlen(token)] = buffernum[0];
//----------------------------------------------------------------------

 	 int hash = generate_hash(tokenbuffer, SymbolTable->size);
   	 comp_dict_item_t *exists = dict_item_get(SymbolTable->data[hash], tokenbuffer);
	if(!exists){
		content *item_content = (content*)malloc(sizeof(content));

		item_content->line = line;		// verificar ponteiro -> certo pq line é var
		item_content->type = type;		// verificar ponteiro -> certo pq type é var

		item_content->associated_type = -1;		// Aquii Etapa 4      !!!!!!!
		item_content->flag_s = -1;		// Aquii Etapa 4      !!!!!!!
		item_content->escopo_global = -1;		// Aquii Etapa 5      !!!!!!!

		//item_content->inferred_type = -1;		// Aqui Etapa 4
		comp_dict_item_t *item;
		switch (type){

		case SIMBOLO_LITERAL_INT:
				item_content->data.integer_value = atoi(token);
				break;
		case SIMBOLO_LITERAL_FLOAT:
				item_content->data.float_value = atof(token);
				break;
		case SIMBOLO_LITERAL_CHAR:
				item_content->data.char_value = token[0];
				break;
		case SIMBOLO_LITERAL_STRING:		
				item_content->data.string_value = strdup(token);	
		item_content->flag_s = 1;	
				//strcpy(item_content->data.string_value, token);			
				break;
		case SIMBOLO_LITERAL_BOOL:		
				if(strcmp(token,"false") == 0){	//printf("Sou um false\n");
					item_content->data.bool_value = 0;
					break;
				}
				if(strcmp(token,"true") == 0){	//printf("Sou um true\n");
					item_content->data.bool_value = 1;
					break;
				}				
	
		case SIMBOLO_IDENTIFICADOR:
				item_content->data.string_value =strdup(token);
		item_content->flag_s = 1;
				//strcpy(item_content->data.string_value, token);					
				break;						
		default:
				printf("Sou um caso não esperado\n");
		
	}

		item = dict_put(SymbolTable, tokenbuffer, (void*)item_content);
		free(tokenbuffer);
		return 	(void*)item;

	}
	else {	
		content *item_content;
		item_content = exists->value;	
		item_content -> line = line;
		//item_content->associated_type = -1;		// Aquii Etapa 4      !!!!!!!
		free(tokenbuffer);
		return (void*)exists;
		
	}

}

comment_line(char* line)
{
	int j;
	j = 0;
	while(line[j] != '\0')
	{
		if(line[j] == '\n')
			lineNumber++;
		j++;
		
	}
}

// caso " ou ' na string
void quotation_marks_removal(char* line, int size, char* buffer) {

	int j = 0;
	int k = 0;
	

	for(k = 0; k<size ; k++)
	{
		if(k != 0 && k !=size-1){
			buffer[j] = line[k];
			j = j + 1;
		}

		//if(k==(size-1))
			//buffer[j] = '\0';									
	}

}

%}
%x AUX_STATE_1
digit [0-9]

%%
int					return TK_PR_INT;
float				return TK_PR_FLOAT;
bool				return TK_PR_BOOL;
char				return TK_PR_CHAR;
string				return TK_PR_STRING;
if					return TK_PR_IF;
then				return TK_PR_THEN;
else				return TK_PR_ELSE;
while				return TK_PR_WHILE;
do					return TK_PR_DO;
input				return TK_PR_INPUT;
output				return TK_PR_OUTPUT;
return				return TK_PR_RETURN;
const				return TK_PR_CONST;
static				return TK_PR_STATIC;
foreach				return TK_PR_FOREACH;
for					return TK_PR_FOR;
switch				return TK_PR_SWITCH;
case				return TK_PR_CASE;
break				return TK_PR_BREAK;
continue			return TK_PR_CONTINUE;
class				return TK_PR_CLASS;
private				return TK_PR_PRIVATE;	
public				return TK_PR_PUBLIC;
protected			return TK_PR_PROTECTED;

"/*"((\*+[^/*])|([^*]))*\**"*/"	comment_line(yytext);


"/"				return yytext[0];
"*"				return yytext[0];
\,				return yytext[0];
\:				return yytext[0];
\;				return yytext[0];
\(				return yytext[0];
\)				return yytext[0];
\[				return yytext[0];
\]				return yytext[0];
\{				return yytext[0];
\}				return yytext[0];
\+				return yytext[0];
\-				return yytext[0];
\<				return yytext[0];
\>				return yytext[0];
\=				return yytext[0];
\!				return yytext[0];
\&				return yytext[0];
\$				return yytext[0];
\%				return yytext[0];
\#				return yytext[0];
\^				return yytext[0];
\<\=				return TK_OC_LE;
\>\=				return TK_OC_GE;
\=\=				return TK_OC_EQ;
\!\=				return TK_OC_NE;
\&\&				return TK_OC_AND;
\|\|				return TK_OC_OR;
\>\>				return TK_OC_SR;
\<\<				return TK_OC_SL;
\n				lineNumber++;
[ \t]+				// Espaço em branco e tab

"false"		{
			yylval.valor_simbolo_lexico = (struct comp_dict_item_t*)insertSymbolOnTable(yytext,lineNumber, SIMBOLO_LITERAL_BOOL);

			return TK_LIT_FALSE;
			}
"true"		{

		yylval.valor_simbolo_lexico = (struct comp_dict_item_t*)insertSymbolOnTable(yytext,lineNumber, SIMBOLO_LITERAL_BOOL);
			return TK_LIT_TRUE;
		}	

[a-zA-Z_][a-zA-Z0-9_]*	{	

				yylval.valor_simbolo_lexico = (struct comp_dict_item_t*)insertSymbolOnTable(yytext,lineNumber, SIMBOLO_IDENTIFICADOR);
				//yylval.string_value = strdup(yytext);	
				return TK_IDENTIFICADOR;	}
	
[0-9]+ 		{	
				yylval.valor_simbolo_lexico =(struct comp_dict_item_t*) insertSymbolOnTable(yytext,lineNumber, SIMBOLO_LITERAL_INT);
						//yylval.integer_value = atoi(yytext);				
				return TK_LIT_INT; }
	
[-+]?{digit}+\.{digit}+ {	
				yylval.valor_simbolo_lexico =(struct comp_dict_item_t*) insertSymbolOnTable(yytext,lineNumber, SIMBOLO_LITERAL_FLOAT);
				//yylval.float_value = atof(yytext);				
				return TK_LIT_FLOAT; }

'.' {			char *buffer = (char *)malloc(sizeof(char)*(yyleng-2)) ;
				quotation_marks_removal(yytext,yyleng,buffer);
				yylval.valor_simbolo_lexico =(struct comp_dict_item_t*) insertSymbolOnTable(buffer,lineNumber, SIMBOLO_LITERAL_CHAR);
				//yylval.string_value = strdup(yytext);
				//yylval.string_value = yytext[0];	// usar strdup ?
				free(buffer);
				return TK_LIT_CHAR; }

\"(\\.|[^"])*\"		{
				char *buffer = (char *)malloc(sizeof(char)*(yyleng-2)) ;
				quotation_marks_removal(yytext,yyleng,buffer);
				yylval.valor_simbolo_lexico =(struct comp_dict_item_t*) insertSymbolOnTable(buffer,lineNumber, SIMBOLO_LITERAL_STRING);
				//yylval.string_value = strdup(yytext);
				free(buffer);

				return TK_LIT_STRING;
}

"//".*  			{}

.	{	//for(i=0;i<yyleng;i++){
		//	printf("%c",yytext[i]);		
		//}
		//printf("\tErro na linha:%d\t Caracteres:%d\n",lineNumber,yyleng);
 		return TOKEN_ERRO;

	}
%%
