/*
  Coloque aqui o identificador do grupo e dos seus membros
*/
%{
#include <stdio.h>
#include "../include/cc_dict.h"
#include "../include/cc_tree.h"
#include "../include/cc_ast.h"

extern comp_tree_t* root;			// Raiz da arvore (usar ?)

%}

%union {	

	struct  comp_dict_item *valor_simbolo_lexico;
	struct  comp_tree_t *astTree;		/// Aqui modificado
}
%start program
/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR

%token <valor_simbolo_lexico> TK_LIT_INT	"Numero"
%token <valor_simbolo_lexico> TK_LIT_FLOAT
%token <valor_simbolo_lexico> TK_LIT_FALSE	
%token <valor_simbolo_lexico> TK_LIT_TRUE	
%token <valor_simbolo_lexico> TK_LIT_CHAR
%token <valor_simbolo_lexico> TK_LIT_STRING
%token <valor_simbolo_lexico> TK_IDENTIFICADOR "Identificador"

%token TOKEN_ERRO


//------------------------------Etapa 3 -----------------------------------------------
// Devemos colocar o tipo para todos os não terminais (Pagina 70 manual bison) 
// O professor disse no email que não tinha que ser asssim .... mas.... 
%type <astTree> program
%type <astTree> decList "decList"
%type <astTree> dec 
%type <astTree> input
%type <astTree> output
%type <astTree> outputList
%type <astTree> shiftdecr 
%type <astTree> shiftvaluer
%type <astTree> shiftdecl
%type <astTree> shiftvaluel
%type <astTree> funcallarg
%type <astTree> ifdec 
%type <astTree> simplecommandcall
%type <astTree> return
%type <astTree> whiledec
%type <astTree> dowhiledec
%type <astTree> userTypeAttr
%type <astTree> locvardec
%type <astTree> varInit
%type <astTree> value
%type <astTree> funcdec
%type <valor_simbolo_lexico> header
%type <astTree> body
%type <astTree> simplebody
%type <astTree> commandblockdec
%type <astTree> simplecommand
%type <astTree> expression
%type <astTree> attrdec 
%type <astTree> primitiveTypeAttr
%type <astTree> exp2
%type <astTree> funcalldec
//------------------------------------------------------------------------------------

%left TK_OC_AND TK_OC_OR
%left  TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
%left '<' '>' '!'
%left '-' '+'
%left '*' '/' 
%left UMINUS

%nonassoc TK_PR_THEN PR
%nonassoc TK_PR_ELSE 


%precedence TK_PR_OUTPUT
%precedence '}'
%precedence ';'
%precedence TK_IDENTIFICADOR
%precedence '('
%precedence ','



%%
//-----------------------------------------------------------------------------------------
//PROGRAMA
// Program do tipo astTree ou extern de root: root = $1 ???
program:    decList 
        {
		if($1 != NULL){
        node_value *valor = malloc(sizeof(node_value));
        valor->node_type=AST_PROGRAMA;
        valor->SymbolTableEntry = NULL;
        $$ = tree_make_node((void*)valor);
        comp_tree_t *child = getFirstElement($1); //implementar: $1 é o ultimo argumento, voltar nos ponteiros 
         if(child != NULL)
             tree_insert_node($$, child);
        }else{
			{ $$=NULL;};
			}			
		};  

//decList: decList dec 
decList: dec decList  
         {
		    if($2 == NULL)
		          $$ = $1;
		     else if($1 == NULL)
		                $$ = $2;
		          else
		            {
		                tree_insert_node($1, $2); // de esquerda a direita
		                 $$ = $1;		//---------------------999
		            }	
		}
                |/*nothing*/ {$$ =NULL;}
	        ;						
		
dec:   	globalvardec  {$$=NULL;}
        | usertypedec {$$=NULL;}
		| funcdec {$$ = $1;} 
	; 

//----------------------------------------------------------------------------------------
//STATIC
staticprec:	TK_PR_STATIC
		|//nothing
		; 

types:          prType 
		| userType
		;

prType: TK_PR_INT
		|TK_PR_FLOAT
		|TK_PR_BOOL 		
		|TK_PR_CHAR
		|TK_PR_STRING		
	        ;	

userType:       TK_IDENTIFICADOR ;

//-----------------------------------------------------------------------------------------
//DECLARACAO DE VARIAVEL GLOBAL
globalvardec:   staticprec types TK_IDENTIFICADOR varVector ';';

varVector:      '['TK_LIT_INT']' // Como garantir que é positivo ? Novo tipo Natural ?
 		|//nothing
		;
//-----------------------------------------------------------------------------------------
//TIPO DO USUARIO - a lista de parametros nao pode ser vazia
usertypedec:    TK_PR_CLASS TK_IDENTIFICADOR '['fieldList']' ';'; 

fieldList:	fieldList field 		
 		| fieldList ':'
		|//nothing 
		;

field:          fieldEncap fieldType fieldId ';';  
                                        
fieldEncap:	TK_PR_PROTECTED
		|TK_PR_PRIVATE
		|TK_PR_PUBLIC	
		;

fieldType:      prType ;

fieldId:	userType ;

//-----------------------------------------------------------------------------------------
//DECLARACAO DE FUNCAO
funcdec:  header simplebody
       {       //AQUI VAI UM GV_DECLARE EM ALGUM LUGAR
		node_value *valor = malloc(sizeof(node_value));
		valor->node_type=AST_FUNCAO;
		valor->SymbolTableEntry = $1;
		$$ = tree_make_node((void*)valor);

        //gv_declare(AST_FUNCAO, $$,$1->key); //nao sei se é assim ---->>>> AQUIII

         tree_insert_node($$, $2); //o professor disse que podia ser um bloco mesmo, arrumar depois se precisar

		}
			; 
header:  staticprec types TK_IDENTIFICADOR '(' headerparam ')' {$$ = $3;}; 

headerparam:    param
		| headerparam ',' param
		|//nothing
		;

param:	        paramprec paramType paramName ;

paramprec:      TK_PR_CONST
		|//nothing
		;

paramType:      prType
		|userType
		;

paramName:      TK_IDENTIFICADOR ;


body:           '{' commandblockdec  pv
                    {
                        node_value *valor = malloc(sizeof(node_value));
                        valor->node_type=AST_BLOCO;
                        valor->SymbolTableEntry = NULL;
                        $$ = tree_make_node((void*)valor);

                            comp_tree_t *child = getFirstElement($2);
                            
                            if(child != NULL)
                                    tree_insert_node($$, child);
                    }
                | '{' ';' pv
                ; 
                
                

pv:             '}'';'
                |'}'
                ;

simplebody:     '{' commandblockdec '}'
            {	printf("simplebody\n");
                $$ = getFirstElement($2);
                /*node_value *valor = malloc(sizeof(node_value));
                valor->node_type=AST_BLOCO;
                valor->SymbolTableEntry = NULL;
                $$ = tree_make_node((void*)valor);

                    comp_tree_t *child = getFirstElement($2);
                    
                    if(child != NULL){
						node_value *temp =child->value;
						printf("Hijo commandblock no NULL\n");//printar child
							printf("tipo:%d\n",temp->node_type);
                            tree_insert_node($$, child);
					}*/
            }
	        ;
//-----------------------------------------------------------------------------------------
//BLOCOS DE COMANDO
commandblockdec: simplecommand  commandblockdec 
                {
                if($2 == NULL)
                       $$ = $1;
                 else if($1 == NULL)
                	      $$ = $2;
                       else
                        {
                           tree_insert_node($1, $2);
                            $$ = $1;//---------------------999
                        }	
		        }
                  |/*nothing*/ {$$=NULL;} 
		  ;

simplecommand:  locvardec ';'{$$=$1;}
                | attrdec ';'{$$=$1;}
	        	| input ';'{$$=$1;}
	     		| output ';'{$$=$1;}
      	        | funcalldec ';' {$$=$1;}
           		| shiftdecr {$$=$1;}
				| shiftdecl {$$=$1;}	
	     		| return ';'{$$=$1;}
	     		| break ';'
	     		| continue ';'
				| body	{$$=$1;}
      	        | ifdec ';' {$$=$1;}
      	        | whiledec ';' {$$=$1;}
                | dowhiledec ';'{$$=$1;}
                | fordec ';'
                | foreachdec ';'
                | switchdec ';'
                | casedec
		;


simplecommandcall: 	locvardec {$$=$1;}
          			| attrdec {$$=$1;} 
     	 			| input {$$=$1;}
     				| output{$$=$1;}
					| funcalldec 
           			| shiftdecr {$$=$1;}
					| shiftdecl {$$=$1;}	
    	 			| return {$$=$1;}
      				| break 		
      				| continue 		
					| simplebody			             	
					| ifdec {$$=$1;}
                    | whiledec  {$$=$1;}
                    | dowhiledec {$$=$1;}
                    | fordec 
                    | foreachdec 
                    | switchdec 
                    | casedec

              ;

//-----------------------------------------------------------------------------------------
// DECLARACAO DE VARIAVEL LOCAL -Verificar atribuição para usertype que deve estar proibida

locvardec:      locType TK_IDENTIFICADOR {$$=NULL;}		/// VERIFICAR
	            |locType TK_IDENTIFICADOR varInit
                       {	
				node_value *value = malloc(sizeof(node_value));
                value->node_type = AST_ATRIBUICAO;
				value->SymbolTableEntry = NULL; 

				node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $2;
				comp_tree_t *arvore = tree_make_node((void*) value_ID);

				$$ = tree_make_binary_node((void*)value,arvore,$3);
        		} ;

locType:        TK_PR_STATIC userType
                | TK_PR_STATIC TK_PR_CONST userType
                | TK_PR_CONST userType
                | userType
                | TK_PR_STATIC prType
                | TK_PR_CONST prType
                | TK_PR_STATIC TK_PR_CONST prType
                | prType
                ;

varInit:        TK_OC_LE value {$$=$2;} //AQUI O VALUE NAO SERIA SÓ EXPRESSION?? Depende  
		//| /*nothing*/ 	Depende de como queremos fazer;int a = a+1 ou int a= 1
		;
                
value:	TK_LIT_INT 
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
			}
        | TK_LIT_FLOAT 
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		} 
        | TK_LIT_CHAR
                {		
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		} 
	|TK_LIT_STRING
                {		
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        | TK_LIT_TRUE	
                {	
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        | TK_LIT_FALSE
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        |TK_IDENTIFICADOR 
                {
			node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_IDENTIFICADOR;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
	; 

//----------------------------------------------------------------------------------------
// EXPRESSOES ARITMETICAS
expression:     TK_LIT_INT 
                        { printf("Expre_INT\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;

				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			}
                | TK_LIT_FLOAT 
                        {
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			} 
                | TK_LIT_CHAR
		        {		
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			} 
                | TK_LIT_STRING
                        {		
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			}
                | TK_LIT_TRUE
                        {		
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			}
                | TK_LIT_FALSE                        
                        {		
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			}
				| expression '*' expression
                        {   
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_MULTIPLICACAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }  
                | expression '-' expression                        
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_SUBTRACAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }
                | expression '+' expression 
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_SOMA;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }
                | expression '/' expression 
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_DIVISAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }
                | expression '>' expression     
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_G;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }
                | expression '<' expression     
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_L;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
                        }
                | '-' expression                
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_INVERSAO;
                		value->SymbolTableEntry = NULL;
				$$ = tree_make_unary_node((void*)value,$2);    
                        }
                | '+' expression               //nao existe nenhuma regras pra isso no ast.h
                | expression TK_OC_LE expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_LE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
                | expression TK_OC_GE expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_GE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
                | expression TK_OC_EQ expression
                        {       
			        node_value *value = malloc(sizeof(node_value));
			        value->node_type = AST_LOGICO_COMP_IGUAL;
			        value->SymbolTableEntry = NULL; 					
			        $$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
                | expression TK_OC_NE expression
                        {      
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_DIF;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
                | expression TK_OC_AND expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_E;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
                | expression TK_OC_OR expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_OU;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);    
                        }
				| '!' expression 
                        {	printf("neg\n");
                                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_NEGACAO;
                		value->SymbolTableEntry = NULL;
				$$ = tree_make_unary_node((void*)value,$2);   
                        }                                              
			| TK_IDENTIFICADOR '['expression']'
                        {
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_VETOR_INDEXADO ;
				value->SymbolTableEntry = NULL;
	                        
		        node_value *value_ID = malloc(sizeof(node_value));
		        value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        		comp_tree_t *identificador = tree_make_node((void*)value_ID);
				
				$$ = tree_make_binary_node((void*)value,identificador,$3);    
			}                                               
                | exp2 {$$=$1;}
		;

exp2:           TK_IDENTIFICADOR 
                        {
				node_value *value_ID = malloc(sizeof(node_value));
                                value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
				$$ = arvore;
			}
		|'(' expression ')' {$$=$2;} 
		|funcalldec {$$=$1;}
                ;

   
//-----------------------------------------------------------------------------------------
// ATRIBUICAO
attrdec:    primitiveTypeAttr {$$=$1;}
	        |userTypeAttr {$$=$1;}
		;

primitiveTypeAttr:  TK_IDENTIFICADOR '=' expression 
                                {	printf("atrib\n");
					node_value *value = malloc(sizeof(node_value));
                    value->node_type = AST_ATRIBUICAO;
					value->SymbolTableEntry = NULL; 

					node_value *value_ID = malloc(sizeof(node_value));
                    value_ID->node_type = AST_IDENTIFICADOR;
					value_ID->SymbolTableEntry = $1;

					comp_tree_t *arvore = tree_make_node((void*) value_ID);
						
					$$ = tree_make_binary_node((void*)value,arvore,$3);
				}
		       | TK_IDENTIFICADOR '['expression']' '=' expression
                           {
					node_value *value = malloc(sizeof(node_value));
                    value->node_type = AST_ATRIBUICAO;
					value->SymbolTableEntry = NULL; 

					node_value *value_id = malloc(sizeof(node_value));
				    value_id->node_type = AST_VETOR_INDEXADO ;
				    value_id->SymbolTableEntry = NULL; 	
	                        
                    node_value *value_id2 = malloc(sizeof(node_value));
                    value_id2->node_type = AST_LITERAL;
				    value_id2->SymbolTableEntry = $1;
        			comp_tree_t *vet_id = tree_make_node((void*)value_id2);		

				    comp_tree_t *vet  = tree_make_binary_node((void*)value_id,vet_id,$3); 

					$$ = tree_make_binary_node((void*)value,vet,$6);
				}
		        ;


userTypeAttr:           TK_IDENTIFICADOR '!' TK_IDENTIFICADOR '=' expression   
                   {

					node_value *value = malloc(sizeof(node_value));
                    value->node_type = AST_ATRIBUICAO;
					value->SymbolTableEntry = NULL; 

					node_value *value_ID = malloc(sizeof(node_value));
                    value_ID->node_type = AST_IDENTIFICADOR;
					value_ID->SymbolTableEntry = $1;
					comp_tree_t *id_1 = tree_make_node((void*) value_ID);

					node_value *value_ID2 = malloc(sizeof(node_value));
                    value_ID2->node_type = AST_IDENTIFICADOR;
					value_ID2->SymbolTableEntry = $3;
					comp_tree_t * id_2 = tree_make_node((void*) value_ID2);
						
					$$ = tree_make_ternary_node((void*)value,id_1,id_1,$5);
				};

//-----------------------------------------------------------------------------------------
//INPUT
input:          TK_PR_INPUT expression 
                        {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_INPUT;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_unary_node((void*)value,$2);    
                       };

//-----------------------------------------------------------------------------------------
//OUTPUT
output:         TK_PR_OUTPUT outputList 
                        {
                                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_OUTPUT;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_node((void*)value);   

                                comp_tree_t *child = getFirstElement($2);
                                
                                if(child != NULL)
                                        tree_insert_node($$, child);  
                        };

outputList:   expression outputList 
                        {
		                if($2 == NULL)
                                        $$ = $1;
                                else if($1 == NULL)
                                        $$ = $2;
                                else
                                {
                                        tree_insert_node($1, $2);
                                        $$ = $1;
                                }	
		        }
	        | ','outputList  {$$ = $2;}
		|/*nothing*/ {$$ = NULL;}
		;

//-----------------------------------------------------------------------------------------
// FUNCTION CALL
funcalldec:     TK_IDENTIFICADOR '('funcallarg')' 
                        {     
                 node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_CHAMADA_DE_FUNCAO;
				value->SymbolTableEntry = NULL;

                node_value *value_id = malloc(sizeof(node_value));
				value_id->node_type = AST_IDENTIFICADOR;
				value_id->SymbolTableEntry = $1;
                comp_tree_t* id = tree_make_node((void*)value_id);		
		
				$$ = tree_make_unary_node((void*)value,id);

                                comp_tree_t *child = getFirstElement($3);
                                
                                if(child != NULL)
                                        tree_insert_node($$, child);
                        };

funcallarg: expression funcallarg 
                        {
		                if($2 == NULL)
                                        $$ = $1;
                                else if($1 == NULL)
                                        $$ = $2;
                                else
                                {
                                        tree_insert_node($1, $2);
                                        $$ = $1;//---------------------999
                                }	
		        }
		|','funcallarg  {$$ = $2;}
		| /*nothing*/ {$$ = NULL;}
		;

//-----------------------------------------------------------------------------------------
// SHIFT RIGHT
shiftdecr:       TK_IDENTIFICADOR shiftsymr shiftvaluer 
                        {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_RIGHT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
    			value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;

				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
		
				$$ = tree_make_binary_node((void*)value,arvore,$3);    
      			};

shiftsymr:       TK_OC_SR
              //  | TK_OC_SL
                ;

shiftvaluer:     TK_LIT_INT 	
                        {
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LITERAL;
				value->SymbolTableEntry = $1;
		
				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value);

				$$ = arvore;
			}
; 

//-----------------------------------------------------------------------------------------
// SHIFT LEFT
shiftdecl:       TK_IDENTIFICADOR shiftsyml shiftvaluel 
                        {             
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_LEFT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
    				value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;

				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
		
				$$ = tree_make_binary_node((void*)value,arvore,$3);    
      			};

shiftsyml:	TK_OC_SL
                ;

shiftvaluel:     TK_LIT_INT 	
                        {
		                node_value *value = malloc(sizeof(node_value));
		                value->node_type = AST_LITERAL;
		                value->SymbolTableEntry = $1;

		                comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
		                arvore = tree_make_node((void*) value);

		                $$ = arvore;
	                };

//-----------------------------------------------------------------------------------------
//RETURN
return:        TK_PR_RETURN expression
                       {        
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_RETURN;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_unary_node((void*)value,$2);    
                        };

//-----------------------------------------------------------------------------------------
//BREAK
break:          TK_PR_BREAK ;
//-----------------------------------------------------------------------------------------
//CONTINUE
continue:       TK_PR_CONTINUE ;
//-----------------------------------------------------------------------------------------
//CASE
casedec:       TK_PR_CASE TK_PR_INT ':';

//-----------------------------------------------------------------------------------------
// IF
ifdec:          TK_PR_IF '('expression')'  TK_PR_THEN simplecommandcall 
                        {       //NA INSERÇÃO É NULL OU UM NODO BINARIO?
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_IF_ELSE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$3, $6);    
                        }
               |TK_PR_IF '('expression')'TK_PR_THEN simplecommandcall TK_PR_ELSE simplecommandcall
                        {              
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_IF_ELSE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_ternary_node((void*)value,$3, $6, $8);    
                        };

//-----------------------------------------------------------------------------------------
// WHILE
whiledec:       TK_PR_WHILE '('expression')' TK_PR_DO simplecommandcall
                        {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_WHILE_DO;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$6,$3);    
                         }  ;


//-----------------------------------------------------------------------------------------
//DOWHILE
dowhiledec:     TK_PR_DO simplecommandcall TK_PR_WHILE '('expression')'
                        {              
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_DO_WHILE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$2, $5);    
                        };
//-----------------------------------------------------------------------------------------

//SWITCH - o comando pode ser qualquer um ou só case?
switchdec:      TK_PR_SWITCH '('expression')' simplecommandcall ;
//-----------------------------------------------------------------------------------------
// FOREACH
foreachdec:     TK_PR_FOREACH '('TK_IDENTIFICADOR':' expression foreachList')' simplecommandcall ;

foreachList:    ','expression
		|//nothing
 		;
//-----------------------------------------------------------------------------------------
// FOR
fordec:         TK_PR_FOR '('simplecommandcall forcommand':' expression':' simplecommandcall forcommand')' simplecommandcall ;

forcommand:     ','simplecommandcall
                |//nothing
 		;
