// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or____
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CC_DICT_H_
#define CC_DICT_H_

#define IKS_INT_VAR    		1
#define IKS_FLOAT_VAR    	2
#define IKS_CHAR_VAR    	3
#define IKS_STRING_VAR    	4
#define IKS_BOOL_VAR    	5

#define IKS_INT_VECTOR    		6
#define IKS_FLOAT_VECTOR    	7
#define IKS_CHAR_VECTOR    		8
#define IKS_STRING_VECTOR    	9
#define IKS_BOOL_VECTOR    		10

#define IKS_INT_FUNCTION    	11
#define IKS_FLOAT_FUNCTION   	12
#define IKS_CHAR_FUNCTION    	13
#define IKS_STRING_FUNCTION    	14
#define IKS_BOOL_FUNCTION    	15

#define IKS_USER_TYPE_VAR    		16
#define IKS_USER_TYPE_VECTOR    	17
#define IKS_USER_TYPE_FUNCTION    	18



#define IKS_SUCCESS 			0
#define IKS_ERROR_UNDECLARED 	1
#define IKS_ERROR_DECLARED 	 	2

#define IKS_ERROR_VARIABLE 		3
#define IKS_ERROR_VECTOR 		4
#define IKS_ERROR_FUNCTION 		5
#define IKS_ERROR_WRONG_TYPE	6
#define IKS_ERROR_STRING_TO_X	7
#define IKS_ERROR_CHAR_TO_X		8

#define IKS_ERROR_MISSING_ARGS 	9
#define IKS_ERROR_EXCESS_ARGS 	10
#define IKS_WRONG_TYPE_ARGS 	11

#define IKS_ERROR_WRONG_PAR_INPUT 		12
#define IKS_ERROR_WRONG_PAR_OUTPUT 		13
#define IKS_ERROR_WRONG_PAR_RETURN  	14

#define INT_SIZE	4
#define FLOAT_SIZE	8
#define BOOL_SIZE	1
#define CHAR_SIZE	1

//***********************************************
// Etapa 5
//***********************************************

#define arit_imediato 	1
#define atribuicao 		2
#define arit_reg 		3
#define arit_litregc	4
#define arit_litexpc	5

#define arit_litvetc	6

#define atribuicao_global 		69


#define lshift_reg	8
#define rshift_reg	9
#define lcomp_reg	10
#define gcomp_reg	11
#define lecomp_reg	12
#define gecomp_reg	13
#define eqcomp_reg	14
#define necomp_reg	15
#define or_reg		16
#define and_reg		17
#define mult_reg	18
#define sub_reg		19
#define div_reg		20
#define neg_reg		21

#define nop_opcode 0
//------------------------------------------------
#define add_opcode 	1
#define sub_opcode 	2
#define mult_opcode 	3
#define div_opcode 	4
#define addI_opcode 	5
#define subI_opcode 	6
#define rsubI_opcode 	7
#define multI_opcode 	8
#define divI_opcode 	9
#define rdivI_opcode 	10
//------------------------------------------------
#define lshift_opcode 	11
#define lshiftI_opcode 	12
#define rshift_opcode 	13
#define rshiftI_opcode 	14
#define and_opcode 	15
#define andI_opcode 	16
#define or_opcode 	17
#define orI_opcode 	18
#define xor_opcode 	19
#define xorI_opcode 	20

#define loadI_opcode 	21
#define load_opcode 	22
#define loadAI_opcode 	23
#define loadAO_opcode 	24

#define store_opcode 	25
#define storeAI_opcode 	26
#define storeAO_opcode 	27

#define cmp_LT_opcode	29
#define cmp_GT_opcode	30
#define cmp_LE_opcode	31
#define cmp_GE_opcode	32
#define cmp_EQ_opcode	33
#define cmp_NE_opcode	34

#define neg_opcode	35

#define cbr 28

#define jumpI_opcode	36


//TODO Continuar LISTA de opcode


/* 
 * Constante: DICT_SIZE, representa o tamanho de uma tabela de símbolos
 */
#define DICT_SIZE 10240

/*
 * Tipo: comp_dict_item_t, é o tipo de uma entrada na tabela de
 * símbolos. O valor do usuário é registrado no campo _value_, de tipo
 * _void *_. Sendo assim, o usuário pode registrar um ponteiro para
 * qualquer tipo de dados, sendo efetivamente uma entrada genérica. A
 * chave _key_ é o identificador único da entrada. O ponteiro _next_ é
 * utilizado casa exista um conflito na função de hash utilizada.
 */
typedef struct comp_dict_item {
  char *key;
  void *value;					// tipo content
  struct comp_dict_item *next;  // ponteiro de overflow
} comp_dict_item_t;

/* 
 * Tipo: comp_dict_t, é o tipo da tabela de símbolos. O campo _size_
 * indica o tamanho total, inicializado para DICT_SIZE e depois não é
 * mais mudado. O campo _occupation_ indica a ocupação atual da
 * tabela, indicando quantos elementos ela contém. O campo _data_ é
 * uma matriz de ponteiros que guarda as entradas.
 */
typedef struct comp_dict {
  int size;
  int occupation;
  int offset_dict; // AQUI AMANDA - Deve ser inicializado(?)
  comp_dict_item_t **data;
} comp_dict_t;

//----------------------------------Etapa 5 ----------------------------
typedef struct tac {
  char *label;	// não sei se precisa  
  int opcode;
  char *op1;
  char *op2;		
  char *op3;
  struct tac *next;
  struct tac *prev;	// Não sei se precisa. 
} tac_i;

//----------------------------------Etapa 4----------------------------
//ESTRUTURA PARA A LISTA DE TABELAS DE SIMBOLO
typedef struct tableList {
	comp_dict_t *table; 		//uma tabela de simbolos
	struct tableList *next;		//tabela de símbolos pai
} table_list;

typedef struct dim_list{
	int dim;
	char  *reg;
	struct dim_list *next;
	struct dim_list *prev;		
} dim_list;

//ESTRUTURA PARA A LISTA DE FUNÇÕES
typedef struct funcList {
	char *name;
	char *paramTypes;
	struct funcList *next;
} func_List;

//--------------------------------------------------------------
union Data {
	int 	integer_value;
	float 	float_value;
	char	char_value;
	char*	string_value;
	int 	bool_value;
};

typedef struct content {
	int line;		//linha da última ocorrência
	int type;		//tipo de token (constantes SIMBOLO_LITERAL_INT.....)
	int associated_type; // ------ Aqui ETAPA 4
	int flag_s;
	int element_size;	//TAMBEM ETAPA 4
	int offset_var; // AQUI AMANDA - Deve ser inicializado(?)
	int dimensoes; 	// Etapa 5
	int type_size;
	int escopo_global;
	struct dim_list* dim_list; // Etapa 5
	union Data data;	//valor do token
}content;


/* Funções: a seguir segue a lista de funções da API cc_dict */

/*
 * Função: dict_new, cria uma nova tabela de símbolos. Retorna um
 * ponteiro para a nova tabela de símbolos ou aborta a execução do
 * programa caso algum erro de alocação de memória tenha ocorrido.
 */
comp_dict_t *dict_new();

/*
 * Função: dict_free, libera a tabela de símbolos alocada previamente
 * com dict_new. Para evitar vazamentos de memória (verifique com
 * valgrind), é importante lembrar que todos as entradas na tabela de
 * símbolos devem ser removidas e liberadas. Caso a ocupação não seja
 * zero, esta função aborta a execução do programa.
 */
void dict_free(comp_dict_t * dict);

/*
 * Função: dict_put, insere uma nova entrada na tabela de
 * símbolos. Recebe três parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos na qual será inserida a nova entrada
 * (este ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da nova entrada na tabela de
 * símbolos; enfim, o parâmetro _value_ é um ponteiro para qualquer
 * tipo ou estrutura de dados, sob responsabilidade do usuário. Esse
 * ponteiro será associado a chave na tabela de símbolos. Caso
 * qualquer um dos parâmetros seja NULL, a função aborta a execução do
 * programa.
 */
void *dict_put(comp_dict_t * dict, char *key, void *value);

/* 
 * Função: dict_get, obtém o valor de uma entrada na tabela de
 * símbolos. Recebe dois parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos da qual será obtida a entrada (este
 * ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da entrada na tabela de
 * símbolos. A função retorna um ponteiro que foi informado no momento
 * do dict_put. É normal fazer um cast para o tipo correto
 * imediatamente após essa chamada de função.  Caso qualquer um dos
 * parâmetros seja NULL, a função aborta a execução do programa.
 */
void *dict_get(comp_dict_t * dict, char *key);

/* 
 * Função: dict_remove, remove o valor de uma entrada na tabela de
 * símbolos. Recebe dois parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos da qual será obtida a entrada (este
 * ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da entrada na tabela de
 * símbolos que será removida. O valor retorno é o ponteiro que foi
 * informado no momento do dict_put, e deve ser liberado pelo usuário.
 * Caso qualquer um dos parâmetros seja NULL, a função aborta a
 * execução do programa.
 */
void *dict_remove(comp_dict_t * dict, char *key);

/*
 * Função: dict_debug_print, usada somente para visualizar os
 * ponteiros do dict.
 */
void dict_debug_print(comp_dict_t * dict);

int search_value(table_list *tableList, char *key);

//int declaration_verification(comp_dict_item_t * id);
int set_associated_type(comp_dict_item_t * id, int tipo);

int set_associated_type2(comp_dict_item_t * id, int tipo);

#endif                          //CC_DICT_H_
