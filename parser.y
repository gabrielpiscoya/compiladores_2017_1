/*
  Coloque aqui o identificador do grupo e dos seus membros
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/cc_dict.h"
#include "../include/cc_tree.h"
#include "../include/cc_ast.h"

extern comp_tree_t* root;			// Raiz da arvore (usar ?)
extern int ret_esperado;
func_List *funcs = NULL;
func_List *args = NULL;
dim_list *diml = NULL;
dim_list *dimac = NULL;
dim_list *dimac_esq = NULL;
tac_i *codigo = NULL;

%}



%union {	

	struct  comp_dict_item *valor_simbolo_lexico;
	struct  comp_tree_t *astTree;		/// Aqui modificado
	int type;
	struct dim_list* dim_list;
	char* label;
	struct cond_label *label_while;
}
%start program
/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR



%token <valor_simbolo_lexico> TK_LIT_INT	
%token <valor_simbolo_lexico> TK_LIT_FLOAT
%token <valor_simbolo_lexico> TK_LIT_FALSE	
%token <valor_simbolo_lexico> TK_LIT_TRUE	
%token <valor_simbolo_lexico> TK_LIT_CHAR
%token <valor_simbolo_lexico> TK_LIT_STRING
%token <valor_simbolo_lexico> TK_IDENTIFICADOR 

%token TOKEN_ERRO


//------------------------------Etapa 3 -----------------------------------------------
// Devemos colocar o tipo para todos os não terminais (Pagina 70 manual bison) 
// O professor disse no email que não tinha que ser asssim .... mas.... 
%type <astTree> program
%type <astTree> decList 
%type <astTree> dec 
%type <astTree> input
%type <astTree> output
%type <astTree> outputList
%type <astTree> shiftdecr 
%type <astTree> shiftvaluer
%type <astTree> shiftdecl
%type <astTree> shiftvaluel
%type <astTree> funcallarg
%type <astTree> ifdec 
%type <astTree> simplecommandcall
%type <astTree> return
%type <astTree> whiledec
%type <astTree> dowhiledec
%type <astTree> userTypeAttr
%type <astTree> locvardec
%type <astTree> varInit
%type <astTree> value
%type <astTree> funcdec
%type <valor_simbolo_lexico> header
%type <astTree> body
%type <astTree> simplebody
%type <astTree> commandblockdec
%type <astTree> simplecommand
%type <astTree> expression
%type <astTree> attrdec 
%type <astTree> primitiveTypeAttr
%type <astTree> exp2
%type <astTree> funcalldec
%type <astTree> dim
%type <astTree> vectordim
%type <type> prType
%type <type> userType
%type <type> types
%type <dim_list> varVector
%type <type> locType
%type <type> paramType
%type <dim_list> dimDec
%type <label> whilecode
%type <label> dowhilecode
//------------------------------------------------------------------------------------

%left TK_OC_AND TK_OC_OR
%left  TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
%left '<' '>' '!'
%left '-' '+'
%left '*' '/' 
%left UMINUS

%nonassoc TK_PR_THEN PR
%nonassoc TK_PR_ELSE 


%precedence TK_PR_OUTPUT
%precedence '}'
%precedence ';'
%precedence TK_IDENTIFICADOR
%precedence '('
%precedence ','



%%
//-----------------------------------------------------------------------------------------
//PROGRAMA

program: novo_escopo decList fim_escopo programa_certo
        {
		//printf("Declist\n");
		if($2 != NULL){
        node_value *valor = malloc(sizeof(node_value));
        valor->node_type=AST_PROGRAMA;
        valor->SymbolTableEntry = NULL;
        $$ = tree_make_node((void*)valor);
        comp_tree_t *child = getFirstElement($2); //implementar: $1 é o ultimo argumento, voltar nos ponteiros 
         if(child != NULL)
             tree_insert_node($$, child);
        }else{
			{ $$=NULL;};
			}			
		};  
programa_certo: /*Nothing*/
			{ //printf("** Análise Semântica Correta **\n");
			
				//exit(IKS_SUCCESS);
			}
novo_escopo: /*Nothing*/ { /*printf("novo escopo\n");*/init_scope();/*printf("sai\n");*/ };
fim_escopo: /*Nothing*/{ /*printf("fim escopo\n");*/end_scope();/*printf("sai\n"); */};
decList: dec decList  
         {	//printf("entrei dec list\n");
		    if($2 == NULL)
		          $$ = $1;
		     else if($1 == NULL)
		                $$ = $2;
		          else
		            {
		                tree_insert_node($1, $2); // de esquerda a direita
		                 $$ = $1;		//---------------------999
		            }	
		}
                |/*nothing*/ {$$ =NULL;}
	        ;						
		
dec:   	globalvardec     { $$=NULL;}
        | usertypedec	 { $$=NULL;}
		| funcdec 		 { $$ = $1;} 
	; 

//----------------------------------------------------------------------------------------
//STATIC
staticprec:	TK_PR_STATIC
		|//nothing
		; 


types:   prType {$$ = $1;}
		| userType {$$ = IKS_USER_TYPE_VAR;}
		;

prType: TK_PR_INT {$$ = IKS_INT_VAR;}
		|TK_PR_FLOAT {$$ = IKS_FLOAT_VAR;}
		|TK_PR_BOOL {$$ = IKS_BOOL_VAR;}	
		|TK_PR_CHAR {$$ = IKS_CHAR_VAR;}
		|TK_PR_STRING {$$ = IKS_STRING_VAR;}	
	        ;		

userType:       TK_IDENTIFICADOR ;

//-----------------------------------------------------------------------------------------
//DECLARACAO DE VARIAVEL GLOBAL

globalvardec:   staticprec types TK_IDENTIFICADOR varVector ';'

		{	//printf("Declaração de variavel global\n");

			if($4 != -1){ //é vetor encadeiar lista e dim

				switch($2){
					case IKS_INT_VAR:
						set_associated_type2($3,IKS_INT_VECTOR);
						break;
					case IKS_FLOAT_VAR:
						set_associated_type2($3,IKS_FLOAT_VECTOR);
						break;
					case IKS_BOOL_VAR:
						set_associated_type2($3,IKS_BOOL_VECTOR);
						break;
					case IKS_CHAR_VAR:
						set_associated_type2($3,IKS_CHAR_VECTOR);
						break;
					case IKS_STRING_VAR:
						set_associated_type2($3,IKS_STRING_VECTOR);
						break;
					case IKS_USER_TYPE_VAR:
						set_associated_type2($3,IKS_USER_TYPE_VECTOR);
						break;					
				}
			}
			else{
				set_associated_type3($3,$2);
			}
			diml=NULL;
		};


//varVector: '['TK_LIT_INT']'{ $$=$2; }// Como garantir que é positivo ? Novo tipo Natural ?
 varVector: '['dimDec']'{ 
					dim_list *aux = diml;
					while(aux!= NULL){
						//printf("Numero:%d esta no ",aux->dim);
						//printf("%s\n",aux->reg);
						aux = aux->next;
					}
						
	
					$$=$2; }// Como garantir que é positivo ? Novo tipo Natural ?
 		|/*nothing*/ {$$=-1;}
		;

dimDec: TK_LIT_INT dimDec {
				int num = 0;
				//content *conteudo = $1->value; 
				num = atoi($1->key);

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);

				strcpy(nome1,$1->key);

				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
			 tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1

				//printf("Número: %d\n",num);

				dim_list *new = (dim_list*)malloc(sizeof(dim_list));
				new-> dim = num;
				new-> reg = reg1;
				new->next = NULL;

				if(diml==NULL){
					//printf("dim vazia\n");
					new->next=NULL;
					diml=new;}
				else{
					//printf("dim com elementos\n");
					new->next = diml;
					diml = new;
				}
			
				} 
	| ',' dimDec		{$$ = $2;}
	| /*Nothing*/		{$$ = NULL;}
	;


//TODO Tratameto para os tipos de usuario São 2 ID que tem que ser verificados na tabela- 

//-----------------------------------------------------------------------------------------
//TIPO DO USUARIO - a lista de parametros nao pode ser vazia
usertypedec:    TK_PR_CLASS TK_IDENTIFICADOR '['fieldList']' ';'; 

fieldList:	fieldList field 		
 		| fieldList ':'
		|//nothing 
		;

field:          fieldEncap fieldType fieldId ';';  
                                        
fieldEncap:	TK_PR_PROTECTED
		|TK_PR_PRIVATE
		|TK_PR_PUBLIC	
		;

fieldType:      prType ;

fieldId:	userType ;

//-----------------------------------------------------------------------------------------
//DECLARACAO DE FUNCAO

funcdec:  header novo_escopo simplebody fim_escopo 
       {       //AQUI VAI UM GV_DECLARE EM ALGUM LUGAR
		//printf("FUNCSAI\n");
		node_value *valor = malloc(sizeof(node_value));
		valor->node_type=AST_FUNCAO;
		valor->SymbolTableEntry = $1;
		$$ = tree_make_node((void*)valor);

        //gv_declare(AST_FUNCAO, $$,$1->key); //nao sei se é assim ---->>>> AQUIII

         tree_insert_node($$, $3); //o professor disse que podia ser um bloco mesmo, arrumar depois se precisar

		}
			; 
header:  staticprec types TK_IDENTIFICADOR '(' headerparam ')' {

		//printf("\tEntrei Header\n");

			char *name = (char*)malloc(sizeof(char)*(strlen($3->key)+1));
			strcpy(name, "");			
			strcpy(name, $3->key);	
			//printf("\t\tNome:%s\n", name);	


			func_List *aux = funcs;
			if(aux == NULL) free(name);
			while(aux!=NULL){

				if(!strcmp(aux->name, "")){
					//printf("Achei nome pendente\n\n");	
					aux->name=name;

				}
				aux=aux->next;
			}

			aux=funcs;
			while(aux!=NULL){
				//printf("\nFunções: %s, Tipo: %s\n", aux->name, aux->paramTypes);
				aux=aux->next;
			}

			switch($2){
					case IKS_INT_VAR:
						ret_esperado = IKS_INT_VAR;
						set_associated_type($3,IKS_INT_FUNCTION);
						break;
					case IKS_FLOAT_VAR:
						ret_esperado =IKS_FLOAT_VAR;
						set_associated_type($3,IKS_FLOAT_FUNCTION);
						break;
					case IKS_BOOL_VAR:
						ret_esperado =IKS_BOOL_VAR;	
						set_associated_type($3,IKS_BOOL_FUNCTION);
						break;
					case IKS_CHAR_VAR:
						ret_esperado =IKS_CHAR_VAR;
						set_associated_type($3,IKS_CHAR_FUNCTION);
						break;
					case IKS_STRING_VAR:
						ret_esperado =IKS_STRING_VAR;
						set_associated_type($3,IKS_STRING_FUNCTION);
						break;
					case IKS_USER_TYPE_VAR:
						ret_esperado =IKS_USER_TYPE_VAR;
						set_associated_type($3,IKS_USER_TYPE_FUNCTION);
						break;					
				}

			$$ = $3;
		
		//printf("Sai Header\n");
}; 

headerparam:    param
		| headerparam ',' param
		|//nothing
		;

param:	        paramprec paramType paramName 
//**************
		{	//printf("\tEntrei Parametros\n");
		func_List *new = (func_List*)malloc(sizeof(func_List));
			func_List *aux;

			char *tipo = (char*)malloc(sizeof(char)*2);
			strcpy(tipo, "");
			sprintf(tipo, "%d", $2);	
			new->paramTypes = tipo;
			new->next=NULL;
			new->name="";

			if(funcs==NULL){
				//printf("Lista funcs vazia\n");
				funcs=new;
			}
			else{	//printf("Lista funcs já tem elemento\n");
				aux=funcs;
				while(aux!=NULL){

					if(aux->next == NULL){
						aux->next = new;
						break;
					}

					aux = aux->next;
				}
			}

		};

paramprec:      TK_PR_CONST
		|//nothing
		;

paramType:      prType { $$=$1;}
				|userType
				;

paramName:      TK_IDENTIFICADOR ;	// Aqui não evemos adicionar a arvore, mas sim a uma lista


body:           novo_escopo'{' commandblockdec  pv fim_escopo

                    {
                        node_value *valor = malloc(sizeof(node_value));
                        valor->node_type=AST_BLOCO;
                        valor->SymbolTableEntry = NULL;
                        $$ = tree_make_node((void*)valor);

                            comp_tree_t *child = getFirstElement($3);
                            
                            if(child != NULL)
                                    tree_insert_node($$, child);
                    }
                | novo_escopo'{' ';' pv fim_escopo
                ; 
                
                

pv:             '}'';'
                |'}'
                ;


simplebody:     '{' commandblockdec '}'// fim_escopo 
            {	//printf("simplebody\n");
                $$ = getFirstElement($2);
//printf("simplebody2\n");
            }
	        ;
//-----------------------------------------------------------------------------------------
//BLOCOS DE COMANDO
commandblockdec: simplecommand  commandblockdec 
                {
                if($2 == NULL)
                       $$ = $1;
                 else if($1 == NULL)
                	      $$ = $2;
                       else
                        {
                           tree_insert_node($1, $2);
                            $$ = $1;//---------------------999
                        }	
		        }
                  |/*nothing*/ {$$=NULL;} 
		  ;

simplecommand:  locvardec ';'{$$=$1;}
                | attrdec ';'{$$=$1;}
	        	| input ';'{$$=$1;}
	     		| output ';'{$$=$1;}
      	        | funcalldec ';' {$$=$1;}
           		| shiftdecr ';' {$$=$1;}
				| shiftdecl ';' {$$=$1;}	
	     		| return ';'{$$=$1;}
	     		| break ';'
	     		| continue ';'
				| body	{$$=$1;}
      	        | ifdec ';' {$$=$1;}
      	        | whiledec ';' {$$=$1;}
                | dowhiledec ';'{$$=$1;}
                | fordec ';'
                | foreachdec ';'
                | switchdec ';'
                | casedec
		;


simplecommandcall: 	locvardec {$$=$1;}
          			| attrdec {$$=$1;} 
     	 			| input {$$=$1;}
     				| output{$$=$1;}
					| funcalldec 
           			| shiftdecr {$$=$1;}
					| shiftdecl {$$=$1;}	
    	 			| return {$$=$1;}
      				| break 		
      				| continue 		
					//| simplebody			             	
					| ifdec {$$=$1;}
                    | whiledec  {$$=$1;}
                    | dowhiledec {$$=$1;}
                    | fordec 
                    | foreachdec 
                    | switchdec 
                    | casedec
					| body

              ;

//-----------------------------------------------------------------------------------------
// DECLARACAO DE VARIAVEL LOCAL -Verificar atribuição para usertype que deve estar proibida

locvardec: 	locType TK_IDENTIFICADOR varInit
               {	
				if($3 == -1 ){
				//	printf("\t\tDeclaração Local -1\n"); 

					set_associated_type($2,$1);//--------------------- Etapa 4
	
					$$=NULL;
		
				}
			else{//if($4 != -1){ //é vetor encadeiar lista e dim
		//	printf("Global\n");
				switch($1){
					case IKS_INT_VAR:
					//	printf("Sou int\n");
						set_associated_type0($2,IKS_INT_VECTOR);
						//printf("ssa\n");
						break;
					case IKS_FLOAT_VAR:
						set_associated_type0($2,IKS_FLOAT_VECTOR);
						break;
					case IKS_BOOL_VAR:
						set_associated_type0($2,IKS_BOOL_VECTOR);
						break;
					case IKS_CHAR_VAR:
						set_associated_type0($2,IKS_CHAR_VECTOR);
						break;
					case IKS_STRING_VAR:
						set_associated_type0($2,IKS_STRING_VECTOR);
						break;
					case IKS_USER_TYPE_VAR:
						set_associated_type0($2,IKS_USER_TYPE_VECTOR);
						break;					
				}
			}
		$$=NULL;
			diml=NULL;
       		} ;

//TODO Usertype São 2 ID que tem que ser verificados na tabela
locType:        TK_PR_STATIC userType 				{$$ = IKS_USER_TYPE_VAR ;}
                | TK_PR_STATIC TK_PR_CONST userType	{$$ = IKS_USER_TYPE_VAR ;}
                | TK_PR_CONST userType				{$$ = IKS_USER_TYPE_VAR ;}
                | userType							{$$ = IKS_USER_TYPE_VAR ;}
                | TK_PR_STATIC prType {$$=$2;}
                | TK_PR_CONST prType  {$$=$2;}
                | TK_PR_STATIC TK_PR_CONST prType {$$=$3;}
                | prType{ $$=$1; }
                ;

varInit: //TK_OC_LE value //{$$=$2;} //AQUI O VALUE NAO SERIA SÓ EXPRESSION?? Depende  
		| varVector    {$$=$1;}
		| /*nothing*/  {$$=-1;}	//Depende de como queremos fazer;int a = a+1 ou int a= 1

		;
                
value:	TK_LIT_INT 
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
			}
        | TK_LIT_FLOAT 
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		} 
        | TK_LIT_CHAR
                {		
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		} 
	|TK_LIT_STRING
                {		
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        | TK_LIT_TRUE	
                {	
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        | TK_LIT_FALSE
                {
            node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_LITERAL;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
        |TK_IDENTIFICADOR 
                {
			node_value *value_ID = malloc(sizeof(node_value));
            value_ID->node_type = AST_IDENTIFICADOR;
			value_ID->SymbolTableEntry = $1;

			comp_tree_t *arvore = tree_make_node((void*)value_ID);
			$$ = arvore;
		}
	; 

//----------------------------------------------------------------------------------------
// EXPRESSOES ARITMETICAS
expression:     TK_LIT_INT 
                 {// printf("Expre_INT\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;

				set_primitive_type($1, IKS_INT_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_INT_VAR ;

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1

                value_ID->reg_resultado = reg1;
						

				$$ = arvore;
			}
                | TK_LIT_FLOAT 
                        {//printf("Expre_FLOAT\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        			set_primitive_type($1, IKS_FLOAT_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);
	
				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_FLOAT_VAR ;
		
				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
			//	printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1
	            value_ID->reg_resultado = reg1;
				$$ = arvore;
			} 
                | TK_LIT_CHAR
		        {		//printf("Expre_CHAR\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        			set_primitive_type($1, IKS_CHAR_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_CHAR_VAR ;
				$$ = arvore;

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1
                value_ID->reg_resultado = reg1;
			} 
                | TK_LIT_STRING
                        {		//printf("Expre_STRING\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
       			set_primitive_type($1, IKS_STRING_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_STRING_VAR ;
				$$ = arvore;

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1
                value_ID->reg_resultado = reg1;
			}
                | TK_LIT_TRUE
                        {	//printf("Expre_TRUE\n");	
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        			set_primitive_type($1, IKS_BOOL_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_BOOL_VAR ;
				$$ = arvore;

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1
                value_ID->reg_resultado = reg1;
			}
                | TK_LIT_FALSE                        
                        {	//	printf("Expre_FALSE\n");
                node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        			set_primitive_type($1, IKS_BOOL_VAR);
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;	
				valor-> inferred_type = IKS_BOOL_VAR ;
				$$ = arvore;

				char *nome1;
				nome1 = (char*)malloc(sizeof($1->key)+1);
				strcpy(nome1,$1->key);
	
				char *reg1 ;
				reg1 = get_reg();
				//printf("Reg: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1
				value_ID->reg_resultado = reg1;
			}
			| expression '*' expression
                        {   
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_MULTIPLICACAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);
				tac_ast($$);
                        }  
                | expression '-' expression                        
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_SUBTRACAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);
				//printf("Pre sub\n");				
				tac_ast($$);

                        }
                | expression '+' expression 
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_SOMA;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);

				tac_ast($$);
	/*		if(codigo!=NULL){
				printf("  opcode %d\n",codigo->opcode);
				printf("  op1 %s\n",codigo->op1);
				printf("  op2 %s\n",codigo->op2);
				printf("  op3 %s\n",codigo->op3);
			}*/
                        }
                | expression '/' expression 
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_DIVISAO;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);
				tac_ast($$);

                        }
                | expression '>' expression     
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_G;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);
				tac_ast($$);
                        }
                | expression '<' expression     
                        {   
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_L;
				value->SymbolTableEntry = NULL;
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$);
				tac_ast($$);
                        }
                | '-' expression                
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_ARIM_INVERSAO;
                		value->SymbolTableEntry = NULL;
				$$ = tree_make_unary_node((void*)value,$2); 
				int i = check_type($$);   
                        }
                | '+' expression               //nao existe nenhuma regras pra isso no ast.h
                | expression TK_OC_LE expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_LE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3); 
				int i = check_type($$);   
                        }
                | expression TK_OC_GE expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_GE;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);
				int i = check_type($$); 
				tac_ast($$);   
                        }
                | expression TK_OC_EQ expression
                        {       
			        node_value *value = malloc(sizeof(node_value));
			        value->node_type = AST_LOGICO_COMP_IGUAL;
			        value->SymbolTableEntry = NULL; 					
			        $$ = tree_make_binary_node((void*)value,$1,$3); 
				int i = check_type($$); 
				tac_ast($$);  
                        }
                | expression TK_OC_NE expression
                        {      
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_DIF;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3);  
				int i = check_type($$); 
				tac_ast($$); 
                        }
                | expression TK_OC_AND expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_E;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3); 
				int i = check_type($$); 
				tac_ast($$);  
                        }
                | expression TK_OC_OR expression
                        {       
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_OU;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_binary_node((void*)value,$1,$3); 
				int i = check_type($$);   
				tac_ast($$);
                        }
				| '!' expression 
                        {	//printf("neg\n");
                                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LOGICO_COMP_NEGACAO;
                		value->SymbolTableEntry = NULL;
				$$ = tree_make_unary_node((void*)value,$2);  
				tac_ast($$); 
                        }                                              
			| TK_IDENTIFICADOR '['vectordim']'
                        {
				//printf("**************************Entrei atrib vetores\n");
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_VETOR_INDEXADO ;
				value->SymbolTableEntry = NULL;
	                        
		        node_value *value_ID = malloc(sizeof(node_value));
		        value_ID->node_type = AST_LITERAL;
				value_ID->SymbolTableEntry = $1;
        		comp_tree_t *identificador = tree_make_node((void*)value_ID);
				
				$$ = tree_make_binary_node((void*)value,identificador,$3);    

				int contdim = 0;
				int cont = 0;
				int tam=0;
				dim_list *aux;
				aux = dimac;
				
				contdim = get_dimensoes($1);				
				while(aux!= NULL){
					//printf("Dimension %d\n",aux->dim);
					aux = aux -> prev;
					cont = cont +1;
					}
				//printf("Dim dec:%d\n",contdim);
				//printf("Dim acesso:%d\n",cont);
				if( contdim!= cont){
					printf("*** ERRO *** Faltam ou sobram dimensões ao acessar\n");
					exit(0);		
				}

				int offset_TK =0;
				offset_TK = get_offset_var($1);
				//int offset_pos=0;
				//int offset_total = 0;
				dim_list *aux_dec;
				dim_list *aux_ac;
				
				aux_dec = get_dim_list($1); // n
				aux_ac  = dimac;			// i  dimac é percorrido com prev
			
				char *regn;
				char *regi;
				char *reg3;
				char *reg4;
				char *reg1;


				//regn = aux_dec -> reg;
				if(aux_dec != NULL)
					aux_dec = aux_dec->next;				

		int n = 1;
	
		while(n<=contdim){

			if(n==2){

				regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;
				regn=aux_dec->reg;
				if(aux_dec != NULL)
					aux_dec = aux_dec->next;				
				reg3=get_reg();
				tac_create(NULL,mult_opcode,regi,regn,reg3);

				regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;
				reg4=get_reg();				
				tac_create(NULL,add_opcode,reg3,regi,reg4);
				
			}
			else 
				if(n!=1){
					reg3=get_last_reg(); // deve ser r4
					regn=aux_dec->reg;
					if(aux_dec != NULL)
						aux_dec = aux_dec->next;					
					reg4=get_reg();
					tac_create(NULL,mult_opcode,reg3,regn,reg4);

					regi=aux_ac->reg;
					if(aux_ac != NULL)
						aux_ac=aux_ac->prev;
					reg3=get_reg();
					tac_create(NULL,add_opcode,reg4,regi,reg3);
										
				}	
			n = n+1;
		}
// caso unidimensional

		if(contdim == 1){
//			regi=get_reg();
			regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;

		}

				tam = get_element_type_size($1);
				char *nome1;
				nome1 = (char*)malloc(sizeof(tam)+1);
				sprintf(nome1, "%d",tam);

				char *pos;
				if(contdim!= 1)
					pos = get_last_reg();
				else{
					  pos = regi;
				}
				reg1 = get_reg();
				//printf("Reg Tam: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1	

				reg3 = get_reg();
				//printf("\n\nReg deslocamento aceso vetor: %s\n",reg3);
	
				tac_create(NULL,mult_opcode,pos,reg1,reg3);	//  i * w

				nome1 = (char*)malloc(sizeof(tam)+1);
				sprintf(nome1, "%d",offset_TK);

				reg1 = get_reg();
				tac_create(NULL,addI_opcode,reg3,nome1,reg1); // base + offset

				reg3 = get_reg();


				tac_create(NULL,loadAO_opcode,"rbss",reg1,reg3);	// Load a[x,x,x]

				dimac = NULL; // Deveriamos dar free -> leak
				value->reg_resultado = reg3;


				}                                               
                | exp2 {$$=$1;}
		;

exp2:           TK_IDENTIFICADOR 
                        {

				node_value *value_ID = malloc(sizeof(node_value));
                value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;
        
				comp_tree_t *arvore = tree_make_node((void*)value_ID);

				node_value *valor = arvore->value;

				comp_dict_item_t *entrada = $1;
				content *conteudo = entrada->value;
							
				//printf("Tipo associado: %d\n",conteudo->associated_type);				

				valor-> inferred_type =  conteudo->associated_type;
// Carregar ID para reg				
				int offset1;
				content* conteudo_f1 = $1 ->value;		//9999999999999999999
	 
				//offset1 = conteudo_f1 -> offset_var;		
				offset1=get_offset_var($1);		
			char *off1 = (char*)malloc(sizeof(char)*5);
				sprintf(off1, "%d",offset1 );

				int global = -1;
				global = get_escopo_global($1);

				char *reg1 ;
				reg1 = get_reg();
				//printf("registrador %s\n\n",reg1);
				if(global != -1)
					tac_create(NULL,loadAI_opcode,"rbss",off1,reg1);	
				else
					tac_create(NULL,loadAI_opcode,"rarp",off1,reg1);	
		
				value_ID->reg_resultado = reg1;
				$$ = arvore;
			}
		|'(' expression ')' {$$=$2;} 
		|funcalldec {$$=$1;
				//int i = check_type($$);
					}
                ;
vectordim: expression vectordim {

			//printf("*--*-*-**********************Entre vetor dim\n");
			comp_tree_t *arvore;
			node_value *valor;
			comp_dict_item_t *entrada;
			int tipo_nodo;
			dim_list *new = (dim_list*)malloc(sizeof(dim_list));

			  if($2 == NULL){
		        $$ = $1;

				arvore = $1;

				valor = arvore->value;
				entrada = valor->SymbolTableEntry;
				if(valor->node_type == AST_LITERAL)	
					new-> dim = atoi(entrada->key);
				else 
					new -> dim = -1;

				new-> reg = valor->reg_resultado;
				new->next = NULL;
				new->prev = NULL;
				if(dimac==NULL){
					//printf("dimac vazia\n");
					new->next=NULL;
					new->prev = NULL;					
					dimac=new;}
				else{
					//printf("dimac com elementos\n");
					dimac ->next = new;
					new->prev = dimac;	
					new->next = NULL;
					dimac = new;
				}

				}
		     else if($1 == NULL)
		                $$ = $2;
		           else
		            {
					arvore = $1;
					valor = arvore->value;
					entrada = valor->SymbolTableEntry;
					//printf("\t*********Dim %s\n\n",entrada->key);
		            tree_insert_node($1, $2);
		            $$ = $1;

				if(valor->node_type == AST_LITERAL)	
					new-> dim = atoi(entrada->key);
				else 
					new -> dim = -1;

				new-> reg = valor->reg_resultado;
				new->next = NULL;
				new->prev = NULL;
				if(dimac==NULL){
					//printf("dimac vazia\n");
					new->next=NULL;
					new->prev = NULL;					
					dimac=new;}
				else{
					//printf("dimac com elementos\n");
					dimac ->next = new;
					new->prev = dimac;	
					new->next = NULL;
					dimac = new;
				}

		
		            }// printf("Sai vetor dim\n");
			}
		 | ',' vectordim { //printf("Vetor dim ,,,,\n");
							$$ = $2;}
		 | /*Nothing*/ {$$ = NULL;}
		 ;
   
//-----------------------------------------------------------------------------------------
// ATRIBUICAO
attrdec:    primitiveTypeAttr {$$=$1;}		// Aqui Etapa 4
	        |userTypeAttr {$$=$1;}
		;

primitiveTypeAttr:  TK_IDENTIFICADOR '=' expression 
                {	
					//printf("*************Entrei atrib primitiva\n");
					node_value *value = malloc(sizeof(node_value));
                    value->node_type = AST_ATRIBUICAO;
					value->SymbolTableEntry = NULL; 

					node_value *value_ID = malloc(sizeof(node_value));
                    value_ID->node_type = AST_IDENTIFICADOR;
					value_ID->SymbolTableEntry = $1;

					comp_tree_t *arvore = tree_make_node((void*) value_ID);
						
					$$ = tree_make_binary_node((void*)value,arvore,$3);
					
					int type = declaration_verification($1);	//-------- Etapa 4

				int offset1;
				content* conteudo_f1 = $1 ->value;
	 
				//offset1 = conteudo_f1 -> offset_var;		//99999999999999	
				offset1=get_offset_var($1);	
				char *off1 = (char*)malloc(sizeof(char)*5);
				sprintf(off1, "%d",offset1 );
				
				int global = -1;
				global = get_escopo_global($1);

			//	printf("Global%d\n",global);
				char *reg1 ;
				reg1 = get_reg();
				//printf("\tRegistrador %s\n\n",reg1);
				//printf("\tOffset %s\n\n",off1);
				if(global != -1){	// Tem que pegar o r do escopo global por isso da errado
					tac_create(NULL,loadAI_opcode,"rbss",off1,reg1);	
					//printf("Global:%d\n ",global);
				}else{
					tac_create(NULL,loadAI_opcode,"rarp",off1,reg1);	
					//printf("Global Else:%d\n ",global);
				}
				value_ID->reg_resultado = reg1;

////******

//ID deve ser carregado para reg
					tac_ast($$);
	//printf("  opcode %d\n",codigo->opcode);
	//printf("  op1 %s\n",codigo->op1);
	//printf("  op2 %s\n",codigo->op2);
	//printf("  op3 %s\n",codigo->op3);



	// Verificcar se esse identificador já foi declarado
				//printf("***************************************Tipo ID :%d\n",type);
		if(type >= IKS_INT_FUNCTION  && type <= IKS_BOOL_FUNCTION){
			//printf(" * ERRO * Identificador deve ser usado como função Linha:%d\n",get_line($1));
	exit(IKS_ERROR_FUNCTION);
		}
		if(type >= IKS_INT_VECTOR  && type <= IKS_BOOL_VECTOR){
			//printf(" * ERRO * Identificador deve ser usado como vetor Linha:%d\n",get_line($1));
	exit(IKS_ERROR_VECTOR);
		}
		
//----------------------------------------------------------------------------------------------
//		Aqui podemos utilziar o campo inferred_type da ED node_value , assim não chamamos a 
//			função novamente
//---------------------------------------------------------------------------------------------		

		if((check_type($3) == IKS_STRING_VAR) && type != check_type($3)){
			printf(" * ERRO * Coerção impossível do tipo string Linha: %d", get_line($1));
			exit(IKS_ERROR_STRING_TO_X);
		}
		else if((check_type($3) == IKS_CHAR_VAR) && type != check_type($3)){
			printf(" * ERRO * Coerção impossivel do tipo char Linha: %d", get_line($1));
			exit(IKS_ERROR_CHAR_TO_X);
		}
		else if((type == IKS_CHAR_VAR) && type != check_type($3)){
			printf(" * ERRO * Tipos incompatíveis Linha: %d", get_line($1));
			exit(IKS_ERROR_WRONG_TYPE);
		}
		else if((type == IKS_STRING_VAR) && type != check_type($3)){
			printf(" * ERRO * Tipos incompatíveis Linha: %d", get_line($1));
			exit(IKS_ERROR_WRONG_TYPE);
		}
		}


		| TK_IDENTIFICADOR '['dim']' '=' expression
                           {

				//printf("****************** Entrei verificação\n");
				node_value *value = malloc(sizeof(node_value));
                value->node_type = AST_ATRIBUICAO;
				value->SymbolTableEntry = NULL; 

				node_value *value_id = malloc(sizeof(node_value));
			    value_id->node_type = AST_VETOR_INDEXADO ;
			    value_id->SymbolTableEntry = NULL; 	
                        
                node_value *value_id2 = malloc(sizeof(node_value));
                value_id2->node_type = AST_LITERAL;
			    value_id2->SymbolTableEntry = $1;
    			comp_tree_t *vet_id = tree_make_node((void*)value_id2);		

			    comp_tree_t *vet  = tree_make_binary_node((void*)value_id,vet_id,$3); 

				$$ = tree_make_binary_node((void*)value,vet,$6);

				int type = declaration_verification($1);
				
				//printf("***************************************Tipo ID :%d\n",type);

//----------------------------------------------------------------------------------------------
//		Aqui podemos utilziar o campo inferred_type da ED node_value , assim não chamamos a 
//			função novamente
//---------------------------------------------------------------------------------------------

	if((check_type($3) == IKS_STRING_VAR) && type != check_type($3)){
			printf(" * ERRO * Coerção impossível do tipo string Linha: %d", get_line($1));
				exit(IKS_ERROR_STRING_TO_X);
		}
	else if((check_type($3) == IKS_CHAR_VAR) && type != check_type($3)){
				printf(" * ERRO * Coerção impossivel do tipo char Linha: %d", get_line($1));
					exit(IKS_ERROR_CHAR_TO_X);
			}
			else if((type == IKS_CHAR_VAR) && type != check_type($3)){
					printf(" * ERRO * Tipos incompatíveis Linha: %d", get_line($1));
						exit(IKS_ERROR_WRONG_TYPE);
					}
					else if((type == IKS_STRING_VAR) && type != check_type($3)){
						printf(" * ERRO * Tipos incompatíveis Linha: %d", get_line($1));
						exit(IKS_ERROR_WRONG_TYPE);
					}

	if(type >= IKS_INT_FUNCTION  && type <= IKS_BOOL_FUNCTION){
			printf(" * ERRO * Identificador deve ser usado como função Linha:%d\n",get_line($1));
			exit(IKS_ERROR_FUNCTION);
			}
	if(type <= IKS_BOOL_VAR){
		printf(" * ERRO * Identificador deve ser usado como variavel Linha:%d\n",get_line($1));
		exit(IKS_ERROR_VARIABLE);
			}		

				int contdim = 0;
				int cont = 0;
				int tam=0;
				dim_list *aux;
				aux = dimac_esq;
				
				contdim = get_dimensoes($1);				
				while(aux!= NULL){
					//printf("Dimension %d\n",aux->dim);
					aux = aux -> prev;
					cont = cont +1;
					}
				//printf("Dim dec:%d\n",contdim);
				//printf("Dim acesso:%d\n",cont);
				if( contdim!= cont){
					printf("*** ERRO *** Faltam ou sobram dimensões ao acessar\n");
					exit(0);		
				}

				int offset_TK =0;
				offset_TK = get_offset_var($1);
				//int offset_pos=0;
				//int offset_total = 0;
				dim_list *aux_dec;
				dim_list *aux_ac;
			
				aux_dec = get_dim_list($1); // n
				aux_ac  = dimac_esq;			// i  dimac é percorrido com prev
			
				char *regn;
				char *regi;
				char *reg3;
				char *reg4;
				char *reg1;


				//regn = aux_dec -> reg;
				if(aux_dec != NULL)
					aux_dec = aux_dec->next;				

		int n = 1;
	
		while(n<=contdim){

			if(n==2){

				regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;
				regn=aux_dec->reg;
				if(aux_dec != NULL)
					aux_dec = aux_dec->next;				
				reg3=get_reg();
				tac_create(NULL,mult_opcode,regi,regn,reg3);

				regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;
				reg4=get_reg();				
				tac_create(NULL,add_opcode,reg3,regi,reg4);
				
			}
			else 
				if(n!=1){
					reg3=get_last_reg(); // deve ser r4
					regn=aux_dec->reg;
					if(aux_dec != NULL)
						aux_dec = aux_dec->next;					
					reg4=get_reg();
					tac_create(NULL,mult_opcode,reg3,regn,reg4);

					regi=aux_ac->reg;
					if(aux_ac != NULL)
						aux_ac=aux_ac->prev;
					reg3=get_reg();
					tac_create(NULL,add_opcode,reg4,regi,reg3);
										
				}	
			n = n+1;
		}
// caso unidimensional

		if(contdim == 1){
//			regi=get_reg();
			regi=aux_ac->reg;
				if(aux_ac != NULL)
					aux_ac=aux_ac->prev;

		}

				tam = get_element_type_size($1);
				char *nome1;
				nome1 = (char*)malloc(sizeof(tam)+1);
				sprintf(nome1, "%d",tam);

				char *pos;
				if(contdim!= 1)
					pos = get_last_reg();
				else{
					  pos = regi;
				}
				reg1 = get_reg();
				//printf("Reg Tam: %s\n",reg1);
				tac_create(NULL,loadI_opcode,nome1,NULL,reg1);	// r1 <= c1	

				reg3 = get_reg();
				//printf("\n\nReg deslocamento aceso vetor: %s\n",reg3);
	
				tac_create(NULL,mult_opcode,pos,reg1,reg3);	//  i * w

				nome1 = (char*)malloc(sizeof(tam)+1);
				sprintf(nome1, "%d",offset_TK);

				reg1 = get_reg();
				tac_create(NULL,addI_opcode,reg3,nome1,reg1); // base + offset

				//reg3 = get_reg();

				//tac_create(NULL,loadAO_opcode,"rarp",reg1,reg3);	// Load a[x,x,x]

				dimac_esq = NULL; // Deveriamos dar free -> leak

				//value->reg_resultado = reg3;

				node_value *valor = malloc(sizeof(node_value));

				comp_tree_t *exp = $6;
				valor =exp->value;

				char *regt;
				regt =valor->reg_resultado;

				int novo=-1;

				novo= get_escopo_global($1); 
				//printf("novo %d\n",novo);
			//	printf("Registrador de resultado expressão %s\n",regt);
				if(novo == -1) {

				tac_create(NULL,storeAO_opcode,regt,"rarp",reg1);	// Load a[x,x,x]
		
				}
				else		
				tac_create(NULL,storeAO_opcode,regt,"rbss",reg1);	// Load a[x,x,x]

			//tac_ast($$);
	//printf("  opcode %d\n",codigo->opcode);
//	printf("  op1 %s\n",codigo->op1);
//	printf("  op2 %s\n",codigo->op2);
//	printf("  op3 %s\n",codigo->op3);

				} ;
dim:  expression dim{
			//printf("*--*-*-**********************Entrei vetor dim ESQUERDA\n");
			comp_tree_t *arvore;
			node_value *valor;
			comp_dict_item_t *entrada;
			int tipo_nodo;
			dim_list *new = (dim_list*)malloc(sizeof(dim_list));

                if($2 == NULL){
                    $$ = $1;
				arvore = $1;

				valor = arvore->value;
				entrada = valor->SymbolTableEntry;
				if(valor->node_type == AST_LITERAL)	
					new-> dim = atoi(entrada->key);
				else 
					new -> dim = -1;

				new-> reg = valor->reg_resultado;
				new->next = NULL;
				new->prev = NULL;
				if(dimac_esq==NULL){
					//printf("dimac vazia\n");
					new->next=NULL;
					new->prev = NULL;					
					dimac_esq=new;}
				else{
					//printf("dimac com elementos\n");
					dimac_esq ->next = new;
					new->prev = dimac_esq;	
					new->next = NULL;
					dimac_esq = new;
				}


				}
                 else if($1 == NULL)
                            $$ = $2;
                       else
                        {
					arvore = $1;
					valor = arvore->value;
					entrada = valor->SymbolTableEntry;
					//printf("\t*********Dim %s\n\n",entrada->key);
                        tree_insert_node($1, $2);
                        $$ = $1;
				if(valor->node_type == AST_LITERAL)	
					new-> dim = atoi(entrada->key);
				else 
					new -> dim = -1;

				new-> reg = valor->reg_resultado;
				new->next = NULL;
				new->prev = NULL;
				if(dimac_esq==NULL){
					//printf("dimac vazia\n");
					new->next=NULL;
					new->prev = NULL;					
					dimac_esq=new;}
				else{
					//printf("dimac com elementos\n");
					dimac_esq ->next = new;
					new->prev = dimac_esq;	
					new->next = NULL;
					dimac_esq = new;
				}

                        }// printf("Sai vetor dim  ESQUERDA\n");	
		
				}
	| ',' dim { $$ = $2 ;}
	| /*Nothing*/ {$$ = NULL;}
	;


userTypeAttr:           TK_IDENTIFICADOR '!' TK_IDENTIFICADOR '=' expression   
                   {

					node_value *value = malloc(sizeof(node_value));
                    value->node_type = AST_ATRIBUICAO;
					value->SymbolTableEntry = NULL; 

					node_value *value_ID = malloc(sizeof(node_value));
                    value_ID->node_type = AST_IDENTIFICADOR;
					value_ID->SymbolTableEntry = $1;
					comp_tree_t *id_1 = tree_make_node((void*) value_ID);

					node_value *value_ID2 = malloc(sizeof(node_value));
                    value_ID2->node_type = AST_IDENTIFICADOR;
					value_ID2->SymbolTableEntry = $3;
					comp_tree_t * id_2 = tree_make_node((void*) value_ID2);
						
					$$ = tree_make_ternary_node((void*)value,id_1,id_1,$5);
				};

//-----------------------------------------------------------------------------------------
//INPUT
input:          TK_PR_INPUT expression 
                        { // printf("Entrei Input\n");
				int i;
				i = expression_verification($2,10);
				if(i != 1){
			printf("* ERRO * Parâmetro input deve ser Identificador Linha: %d\n", tree_line($2));
				exit(IKS_ERROR_WRONG_PAR_INPUT);
				}             
					
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_INPUT;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_unary_node((void*)value,$2);    
                       };

//-----------------------------------------------------------------------------------------
//OUTPUT
output:         TK_PR_OUTPUT outputList 
                        {



                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_OUTPUT;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_node((void*)value);   

                                comp_tree_t *child = getFirstElement($2);
                                
                                if(child != NULL)
                                        tree_insert_node($$, child);  
                        };

outputList:   expression outputList 
                        {

					int f_string = 0;
					int f_arit = 0;

		f_arit = expression_verification($1,12);
		if(f_arit != 1){
			f_arit = expression_verification($1,13);					
		}else 
			if(f_arit != 1){
				f_arit = expression_verification($1,14);
			}
			else 
			  if(f_arit != 1){
				f_arit = expression_verification($1,15);
				}
				else 
				  if(f_arit != 1){
					f_arit = expression_verification($1,16);			
				}	

			//	if(f_arit != 1 ){
//printf("* ERRO * Parâmetro Output deve ser uma Expressão Aritméica ou Uma string  Linha: %d\n", tree_line($1));
	//			exit(IKS_ERROR_WRONG_PAR_OUTPUT);
		//		}  
	
				f_string = string_type_verification($1);
		if(f_string != 1 && f_arit != 1){
			//printf("As flags no erro  foram: a: %d, s: %d \n",f_arit,f_string);
printf("* ERRO * Parâmetro Output deve ser uma Expressão Aritméica ou Uma string  Linha: %d\n", tree_line($1));	
			exit(IKS_ERROR_WRONG_PAR_OUTPUT);			
		}

		//printf("As flags foram: a: %d, s: %d \n",f_arit,f_string);

		              if($2 == NULL)
                                $$ = $1;
                      else if($1 == NULL)
                                 $$ = $2;
                            else
                                {
                                 tree_insert_node($1, $2);
                                 $$ = $1;
                                }	
		        }
	        | ','outputList  {
					//	printf("+ 1 Parâmetro\n");
						$$ = $2;

				}
		|/*nothing*/ {$$ = NULL;}
		;

//-----------------------------------------------------------------------------------------
// FUNCTION CALL
funcalldec:     TK_IDENTIFICADOR '('funcallarg')' 
    		     {     
				//printf("\n\n\nChamada de função\n");
                node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_CHAMADA_DE_FUNCAO;
				value->SymbolTableEntry = NULL;

                node_value *value_id = malloc(sizeof(node_value));
				value_id->node_type = AST_IDENTIFICADOR;
				value_id->SymbolTableEntry = $1;
                comp_tree_t* id = tree_make_node((void*)value_id);		
		
				$$ = tree_make_unary_node((void*)value,id);

                 comp_tree_t *child = getFirstElement($3);
                                
                        if(child != NULL)
                             tree_insert_node($$, child);
				int i = check_type($$); 
				//printf("valorrrrrrrr %d\n",i);

				func_List *aux;

				char *name = (char*)malloc(sizeof(char)*(strlen($1->key)+1));
				strcpy(name, $1->key);

				aux=args;
			if(aux == NULL) free(name);
				while(aux!=NULL){
					//	printf("While args\n");
					if(!strcmp(aux->name, "")){
					//	printf("Achei nome pendente\n");	
						aux->name=name;
					}

					aux=aux->next;
				}
			aux=args;
			while(aux!=NULL){
				//printf("\n\Argumentos: %s, Tipo %s\n", aux->name, aux->paramTypes);
				aux=aux->next;
			}
		//printf("Sai while\n");

				func_List *aux_funcs, *aux_args;
				aux_funcs= funcs;
				aux_args = args;
//********************************************************

				//casos listas vazias
				if( aux_funcs!=NULL && aux_args==NULL ){
						//printf("Caso listas vazias fora while\n");
					//if( !strcmp(aux_funcs->name, name) ){
					printf("* ERRO * Falta de Parâmetros na função. Linha:%d\n",get_line($1));
					exit(IKS_ERROR_MISSING_ARGS);
				//	}

				}

				if( aux_funcs==NULL && aux_args!=NULL  ){
printf("* ERRO * Exceso de Parâmetros na função. Linha:%d\n",get_line($1));						
					exit(IKS_ERROR_EXCESS_ARGS);
				}

//*******************************************************
		//printf("cheguei\n");
				if(aux_funcs!= NULL)
				while( strcmp(aux_funcs->name, name) ){
					//printf("Entrei while func\n");
					aux_funcs=aux_funcs->next;
				}
				if(aux_args!= NULL)
				while( strcmp(aux_args->name, name) ){
					//printf("Entrei while args\n");
					aux_args=aux_args->next;
				}

				int flag=1;
				while(flag && aux_funcs!=NULL && aux_args!=NULL){
						//printf("Caso listas vazias\n");
					//casos listas nao vazias
					if( strcmp(aux_funcs->name, name) && !strcmp(aux_args->name, name) ){
printf("* ERRO * Exceso de Parâmetros na função. Linha:%d\n",get_line($1));						
						exit(IKS_ERROR_EXCESS_ARGS);}

					if( !strcmp(aux_funcs->name, name) && strcmp(aux_args->name, name) ){
					printf("* ERRO * Falta de Parâmetros na função. Linha:%d\n",get_line($1));
						exit(IKS_ERROR_MISSING_ARGS);}

					if( (strcmp(aux_funcs->paramTypes, aux_args->paramTypes)) ){
					printf("* ERRO * Parâmetros de tipo incompatíveis. Linha:%d\n",get_line($1));
						exit(IKS_WRONG_TYPE_ARGS);
					}

					if( !strcmp(aux_funcs->name, name) && !strcmp(aux_args->name, name) && !(strcmp(aux_funcs->paramTypes, aux_args->paramTypes)) ){
						//printf("\n*******  Match ******\n");
						aux_funcs = aux_funcs->next;
						aux_args = aux_args->next;

						//casos listas vazias
						if( aux_funcs!=NULL && aux_args==NULL ){
								//printf("Caso listas vazias\n");
							if( !strcmp(aux_funcs->name, name) ){
					printf("* ERRO * Falta de Parâmetros na função. Linha:%d\n",get_line($1));
								exit(IKS_ERROR_MISSING_ARGS);}
							else{
								flag=0;}
						}

						if( aux_funcs==NULL && aux_args!=NULL && !strcmp(aux_args->name, name) ){
printf("* ERRO * Exceso de Parâmetros na função. Linha:%d\n",get_line($1));						
							exit(IKS_ERROR_EXCESS_ARGS);
						}

					}
/*
					if( strcmp(aux_funcs->name, name) && strcmp(aux_args->name, name) ){
						flag=0;
					}
		*/
				}

				
				//printf("\n\n\SEM ERRO\n\n");
			
                        };                  

funcallarg: expression funcallarg 
                        {
						//printf("Entrei argumentos\n");
		                if($2 == NULL)
                            $$ = $1;
                         else if($1 == NULL)
                                    $$ = $2;
                               else
                                {
                                        tree_insert_node($1, $2);
                                        $$ = $1;//---------------------999
                                }	

				comp_tree_t *nodo = $1;
				node_value *valor;
                valor = nodo->value;
		
				int tipoo = declaration_verification_type(valor->SymbolTableEntry);
		

				//printf("Tipooo: %d\n",tipoo);

				func_List *new = (func_List*)malloc(sizeof(func_List));
				func_List *aux;

				char *tipo = (char*)malloc(sizeof(char)*2);
				strcpy(tipo, "");
				sprintf(tipo, "%d", tipoo);
				new->paramTypes=tipo;
				new->name="";
				new->next=NULL;

				if(args==NULL){
					//printf("Arg vazia\n");
					new->next=NULL;
					args=new;}
				else{
					//printf("Arg com elementos\n");
					new->next = args;
					args = new;
				}

		        }
		|','funcallarg  {
					$$ = $2;
				}
		| /*nothing*/ {$$ = NULL;}
		;

//-----------------------------------------------------------------------------------------
// SHIFT RIGHT
shiftdecr:       TK_IDENTIFICADOR shiftsymr shiftvaluer 
                        {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_RIGHT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
    			value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;

				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
		
				$$ = tree_make_binary_node((void*)value,arvore,$3);  

				tac_ast($$);  
      			};

shiftsymr:       TK_OC_SR
              //  | TK_OC_SL
                ;

shiftvaluer:     TK_LIT_INT 	
                        {
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_LITERAL;
				value->SymbolTableEntry = $1;
		
				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value);

				$$ = arvore;
			}
; 

//-----------------------------------------------------------------------------------------
// SHIFT LEFT
shiftdecl:       TK_IDENTIFICADOR shiftsyml shiftvaluel 
                        {             
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_LEFT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
    				value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = $1;

				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
		
				$$ = tree_make_binary_node((void*)value,arvore,$3);    

				tac_ast($$);
      			};

shiftsyml:	TK_OC_SL
                ;

shiftvaluel:     TK_LIT_INT 	
                        {
		                node_value *value = malloc(sizeof(node_value));
		                value->node_type = AST_LITERAL;
		                value->SymbolTableEntry = $1;

		                comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
		                arvore = tree_make_node((void*) value);

		                $$ = arvore;
	                };

//-----------------------------------------------------------------------------------------
//RETURN
return:        TK_PR_RETURN expression
                       {   
     
				int tipo2 = -1;							

				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_RETURN;
				value->SymbolTableEntry = NULL; 					
				$$ = tree_make_unary_node((void*)value,$2);   

				comp_tree_t *arvore = $2;

				
				node_value *valor = (node_value*)arvore->value;

				tipo2 = valor->inferred_type;
				//printf("\tValor inferido da expressão:%d\n",tipo2);
				
				//printf("\tRet esperado: %d\n",ret_esperado);


// Aplicar coersão
	if(ret_esperado == 1 && (tipo2 == 2 || tipo2 == 5 ) ){

	}
	else 
	if(ret_esperado == 2 && (tipo2 == 1 || tipo2 == 5 ) ){

	}
	else 
	if(ret_esperado == 5 && (tipo2 == 1 || tipo2 == 2 ) ){

	}			
	else 
	if(tipo2 != ret_esperado ){
printf("* ERRO * tipo de expresão de retorno é %d. Esperado %d. Linha: %d\n",tipo2,ret_esperado, tree_line($2));	

				exit(IKS_ERROR_WRONG_PAR_RETURN);
				}

				
                        };

//-----------------------------------------------------------------------------------------
//BREAK
break:          TK_PR_BREAK ;
//-----------------------------------------------------------------------------------------
//CONTINUE
continue:       TK_PR_CONTINUE ;
//-----------------------------------------------------------------------------------------
//CASE
casedec:       TK_PR_CASE TK_PR_INT ':';

//-----------------------------------------------------------------------------------------
// IF
// IF
ifdec:          TK_PR_IF '('expression')'
		{
			char* reg = get_last_reg(); //se nao funcionar pegar o da expression
			char* label1 = get_label();
	
			char* label2 = get_label();
			$<label>$ = label2; 

			//(pula direto pro prox caso true)
			tac_create(NULL, cbr, reg, label1, label2);
			tac_create(label1, nop_opcode, NULL, NULL, NULL);

			
		}	
		TK_PR_THEN simplecommandcall 
                {       
			node_value *value = malloc(sizeof(node_value));
			value->node_type = AST_IF_ELSE;
			value->SymbolTableEntry = NULL; 	

			
			tac_create(($<label>5), nop_opcode, NULL, NULL, NULL);


			$$ = tree_make_binary_node((void*)value,$3, $7);    
                }

              	|TK_PR_IF '('expression')' 
		{	
			char* reg = get_last_reg(); //se nao funcionar pegar o da expression
			char* label1 = get_label();
	
			char* label2 = get_label();
			$<label>$ = label2; 

			//(pula direto pro prox caso true)
			tac_create(NULL, cbr, reg, label1, label2);
			tac_create(label1, nop_opcode, NULL, NULL, NULL);

			
		}
		TK_PR_THEN simplecommandcall TK_PR_ELSE simplecommandcall
                {            
			node_value *value = malloc(sizeof(node_value));
			value->node_type = AST_IF_ELSE;
			value->SymbolTableEntry = NULL; 					
			$$ = tree_make_ternary_node((void*)value,$3, $7, $9);    
                };




//-----------------------------------------------------------------------------------------
// WHILE
whiledec:       TK_PR_WHILE 

			{
			char* label1 = get_label();
			tac_create(label1, nop_opcode, NULL, NULL, NULL);
			$<label>$ = label1;
			}
			'('expression')' 
			

			{
			char* reg = get_last_reg();
			$<label>$ = reg;
			}

			{
			char* label2 = get_label();
			char* label3 = get_label();
			$<label>$ = label3;

			tac_create(NULL, cbr, $<label>6, label2, label3);
			tac_create(label2, nop_opcode, NULL, NULL, NULL);
			}

		 TK_PR_DO simplecommandcall
                        {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_WHILE_DO;
				value->SymbolTableEntry = NULL; 	

				tac_create(NULL, jumpI_opcode, $<label>2, NULL, NULL);
				tac_create($<label>7, nop_opcode, NULL, NULL, NULL);
				
				$$ = tree_make_binary_node((void*)value,$9,$4);  
				
                         }  ;


//-----------------------------------------------------------------------------------------
//DOWHILE
dowhiledec:     TK_PR_DO 
			{
			char* label = get_label();
			tac_create(label, nop_opcode, NULL, NULL, NULL);
			$<label>$ = label;
			}
		 simplecommandcall TK_PR_WHILE '('expression')'
                        {              
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_DO_WHILE;
				value->SymbolTableEntry = NULL; 		
				
				
				char* label_next = get_label();
				char* reg = get_last_reg();
				tac_create(NULL, cbr, reg, $<label>2, label_next);
				tac_create(label_next, nop_opcode,  NULL, NULL, NULL, NULL);
			
				$$ = tree_make_binary_node((void*)value,$3, $6);    
                        };

//-----------------------------------------------------------------------------------------

//SWITCH - o comando pode ser qualquer um ou só case?
switchdec:      TK_PR_SWITCH '('expression')' simplecommandcall ;
//-----------------------------------------------------------------------------------------
// FOREACH
foreachdec:     TK_PR_FOREACH '('TK_IDENTIFICADOR':' expression foreachList')' simplecommandcall ;

foreachList:    ','expression
		|//nothing
 		;
//-----------------------------------------------------------------------------------------
// FOR
fordec:         TK_PR_FOR '('simplecommandcall forcommand':' expression':' simplecommandcall forcommand')' simplecommandcall ;

forcommand:     ','simplecommandcall
                |//nothing
 		;
