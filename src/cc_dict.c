// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cc_dict.h"
#include "cc_list.h"
#include "cc_tree.h"
#include "cc_ast.h"


extern table_list *tableList;
//extern table_list *tableList;

#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_dict, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }


int set_primitive_type(comp_dict_item_t * id, int tipo){
	comp_dict_item_t *entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	valor->associated_type = tipo;
	return 1;
}


// one-at-a-time-hash, Jenkins, 2006
 int generate_hash(char *key, int limit)
{
  unsigned int hash;
  for (hash = 0; *key != '\0'; ++key) {
    hash += *key;
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash % limit;
}

static comp_dict_item_t *dict_item_new()
{
  comp_dict_item_t *item = malloc(sizeof(comp_dict_item_t));
  item->key = NULL;
  item->value = NULL;
  item->next = NULL;
  return item;
}

static void dict_item_free_item(comp_dict_item_t * item)
{
  free(item->key);
 
	content *valor = item->value;
	//printf("aaaaaaFree valor: %d\n",valor-> associated_type);

// Desalocar value e testar se é string


	//printf("\t\t\t%d\n",valor->flag_s);	

	if(valor-> flag_s == 1)
	{	//printf("eis strin\n");
		free(valor->data.string_value);
	}
	
  free(item->value);
  free(item);
}

static void dict_item_free(comp_dict_item_t * item)
{
  if (item == NULL)
    return;

  comp_dict_item_t *ptr = item;
  while (ptr != NULL) {
    ptr = item->next;
    dict_item_free_item(item);
    item = ptr;
  }
}

static void *dict_item_remove(comp_dict_item_t * target, char *key)
{
  comp_dict_item_t *prev = target;
  while (target != NULL && target->key != NULL) {
    if (strcmp(target->key, key) == 0)
      break;
    prev = target;
    target = target->next;
  }
  if (target == NULL)
    return NULL;                // key não existe
  prev->next = target->next;

  void *data = target->value;
  dict_item_free_item(target);
  return data;
}

static int dict_item_insert(comp_dict_item_t * target,
                            comp_dict_item_t * new)
{
  if (target == NULL)
    return 0;
  while (target->next != NULL)
    target = target->next;
  target->next = new;

  return 1;
}

comp_dict_item_t *dict_item_get(comp_dict_item_t * first, char *key)
{
	//printf("Entrei Item Get\n");
  while (first != NULL && first->key != NULL) {
    if (strcmp(first->key, key) == 0)
      return first;
    first = first->next;
  }
  return NULL;
}

static int dict_item_list_print(comp_dict_item_t * item)
{
  int qtd = 0;
  while (item != NULL) {
    ++qtd;
    //printf(" %s %p\n", item->key, item->value);
    item = item->next;
  }
  return qtd;
}

void dict_debug_print(comp_dict_t * dict)
{
  int i, l;
  int qtd = 0;
 // printf("Dict [%d/%d]\n", dict->occupation, dict->size);
  for (i = 0, l = dict->size; i < l; ++i) {
    if (dict->data[i]) {
      printf("%d: %s %p\n", i, dict->data[i]->key, dict->data[i]->value);
      ++qtd;
      if (dict->data[i]->next)
        qtd += dict_item_list_print(dict->data[i]->next);
    }
  }
  //printf("Number of entries: %d\n", qtd);
}

comp_dict_t *dict_new()
{
	//printf("Entrei Dict New\n");
  comp_dict_t *dict = malloc(sizeof(comp_dict_t));
  if (!dict) {
    ERRO("Cannot alocate memory for dict");
    return NULL;
  }
  dict->size = DICT_SIZE;
  dict->occupation = 0;
  dict->offset_dict = 0;	// Utilizado na ETAPA 5 para deslocamento;

  dict->data = malloc(sizeof(comp_dict_item_t *) * dict->size);
  if (!dict->data) {
    free(dict);
    ERRO("Cannot alocate memory for dict data");
    return NULL;
  }

  int i, l = dict->size;
  for (i = 0; i < l; ++i)
    dict->data[i] = NULL;

  return dict;
}

void dict_free(comp_dict_t * dict)
{
	//printf("Entrei dict free\n");
  if (dict->occupation < 0) {	
	return;  }

	//printf("Free maior que zero\n");
  int i, l;
  for (i = 0, l = dict->size; i < l; ++i) {
    if (dict->data[i]) {
      dict_item_free(dict->data[i]);
    }
  }
	//printf("Passei for\n");
		
		//printf("dict:%d\n",dict);
		//printf("dict data:%d\n",dict->data);		
  		free(dict->data);

	 	free(dict);
	
	//printf("Sai free\n");

}

void *dict_put(comp_dict_t * dict, char *key, void *value)
{
  if (dict == NULL || dict->data == NULL || key == NULL) {
    ERRO("At least one parameter is NULL");
  }


  int hash = generate_hash(key, dict->size);
	//printf("Hash:%d\n",hash);

  comp_dict_item_t *newitem = dict_item_new();
  newitem->key = strdup(key);
  newitem->value = value;

  if (dict->data[hash] == NULL) {       // caso 1: entrada não existe na tabela, é inserido imediatamente
    dict->data[hash] = newitem;
    ++dict->occupation;
  } else {                      // caso 2: entrada existe na tabela, inserir no encadeamento
    comp_dict_item_t *exists = dict_item_get(dict->data[hash], key);
    if (!exists) {
      dict_item_insert(dict->data[hash], newitem);
    } else {
		//printf("Não existe\n");
		exists->value=value;	// ------------------------> Aqui modificado
		free(newitem->value);
      dict_item_free_item(newitem);
	
      return (void*)exists;		// -------> Aqui modificado Etapa2
    }
  }
  return (void*)newitem;		// -------> Aqui modificado Etapa2
}

void *dict_get(comp_dict_t * dict, char *key)
{
  if (dict == NULL || dict->data == NULL || key == NULL) {
    ERRO("At least one parameter is NULL");
  }

//printf("Entrei dict get procurando %s\n",key);
  int hash = generate_hash(key, dict->size);
  comp_dict_item_t *item = NULL;

//printf("\tID entrada hash: %d\n",dict->data[hash]);

  if (dict->data[hash]){
 //	printf("\t\t\tEntrada não é NULL\n");
   item = dict_item_get(dict->data[hash], key);
 }
	//printf("retorno\n");
	if(item == NULL)
		return NULL;

 	 return item->value;
}

void *dict_remove(comp_dict_t * dict, char *key)
{
  if (dict == NULL || dict->data == NULL || key == NULL) {
    ERRO("At least one parameter is NULL");
  }


  int hash = generate_hash(key, dict->size);
  comp_dict_item_t *item = NULL;
  void *data = NULL;

  if (dict->data[hash]) {
    if (strcmp(dict->data[hash]->key, key) == 0) {      // chave é primeiro elemento
      item = dict->data[hash];
      data = item->value;
      dict->data[hash] = item->next;
      dict_item_free_item(item);

      if (dict->data[hash] == NULL)     // chave foi ultimo elemento da entrada
        --dict->occupation;
    } else {                    // chave não é primeiro elemento ou não existe
      data = dict_item_remove(dict->data[hash], key);
    }
  }

  return data;
}


//-------------------------------------------------------------------------------------------


//retorna 1 se achar o valor em alguma das tabelas e 0 se não achar
//deve-se enviar a key concatenada com o tipo para essa função
int search_value(table_list *tableList, char *key){

	comp_dict_t *table;
	table_list *aux = tableList;
	int achou = 0;
	
	//aux irá percorrer todas as tabelas de símbolo
	while(aux != NULL && achou==0){

		table = aux->table;

		//se a dict_get achar o valor na tabela
		if( dict_get(table, key) )
			achou=1;

		aux = aux->next;
	}

	return achou;

}
