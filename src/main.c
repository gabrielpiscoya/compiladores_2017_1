#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <cc_ast.h>
#include <cc_dict.h>
#include <string.h>

extern FILE *yyin;
extern char *yytext;
extern int getLineNumber();

extern comp_dict_t *SymbolTable;
extern ret_esperado;
table_list *tableList;
comp_tree_t* root;		// AQUIIIII

// ----- Etapa 5 ------

int num_label;
int num_reg;
extern tac_i *codigo;
extern dim_list *diml;
extern dim_list *dimac;
//---------------------

#define print_nome(TOKEN) printf("%d " #TOKEN " [%s]\n", comp_get_line_number(), yytext);
//#define print_nome2(TOKEN) printf("%d TK_ESPECIAL [%c]\n", comp_get_line_number(), TOKEN);
#define print_nome2(TOKEN) printf("%d %c\n", comp_get_line_number(), TOKEN);
#define USER_INIT main_init (argc, argv);
#define USER_FINALIZE main_finalize ();

void main_avaliacao_etapa_1_tabela (void);
int main_avaliacao_etapa_1 (int argc, char **argv)
{
  int token = 0;
  while (token = yylex()) {
    if (getenv("INF47_TABLE")){
      continue;
    }
    switch (token){
    case ',':
    case ';':
    case ':':
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
    case '+':
    case '-':
    case '*':
    case '/':
    case '<':
    case '>':
    case '=':
    case '!':
    case '&':
    case '$':
    case '%':
    case '#':
    case '^': print_nome2 (token); break;
    case TK_PR_INT: print_nome(TK_PR_INT); break;
    case TK_PR_FLOAT: print_nome(TK_PR_FLOAT); break;
    case TK_PR_BOOL: print_nome (TK_PR_BOOL); break;
    case TK_PR_CHAR: print_nome (TK_PR_CHAR); break;
    case TK_PR_STRING: print_nome (TK_PR_STRING); break;
    case TK_PR_IF: print_nome (TK_PR_IF); break;
    case TK_PR_THEN: print_nome (TK_PR_THEN); break;
    case TK_PR_ELSE: print_nome (TK_PR_ELSE); break;
    case TK_PR_WHILE: print_nome (TK_PR_WHILE); break;
    case TK_PR_DO: print_nome (TK_PR_DO); break;
    case TK_PR_INPUT: print_nome (TK_PR_INPUT); break;
    case TK_PR_OUTPUT: print_nome (TK_PR_OUTPUT); break;
    case TK_PR_RETURN: print_nome (TK_PR_RETURN); break;
    case TK_PR_CONST: print_nome (TK_PR_CONST); break;
    case TK_PR_STATIC: print_nome (TK_PR_STATIC); break;
    case TK_PR_FOREACH: print_nome (TK_PR_FOREACH); break;
    case TK_PR_FOR: print_nome (TK_PR_FOR); break;
    case TK_PR_SWITCH: print_nome (TK_PR_SWITCH); break;
    case TK_PR_CASE: print_nome (TK_PR_CASE); break;
    case TK_PR_BREAK: print_nome (TK_PR_BREAK); break;
    case TK_PR_CONTINUE: print_nome (TK_PR_CONTINUE); break;
    case TK_PR_CLASS: print_nome (TK_PR_CLASS); break;
    case TK_PR_PRIVATE: print_nome (TK_PR_PRIVATE); break;
    case TK_PR_PUBLIC: print_nome (TK_PR_PUBLIC); break;
    case TK_PR_PROTECTED: print_nome (TK_PR_PROTECTED); break;
    case TK_OC_LE: print_nome (TK_OC_LE); break;
    case TK_OC_GE: print_nome (TK_OC_GE); break;
    case TK_OC_EQ: print_nome (TK_OC_EQ); break;
    case TK_OC_NE: print_nome (TK_OC_NE); break;
    case TK_OC_AND: print_nome (TK_OC_AND); break;
    case TK_OC_OR: print_nome (TK_OC_OR); break;
    case TK_OC_SL: print_nome (TK_OC_SL); break;
    case TK_OC_SR: print_nome (TK_OC_SR); break;
    case TK_LIT_INT: print_nome (TK_LIT_INT); break;
    case TK_LIT_FLOAT: print_nome (TK_LIT_FLOAT); break;
    case TK_LIT_FALSE: print_nome (TK_LIT_FALSE); break;
    case TK_LIT_TRUE: print_nome (TK_LIT_TRUE); break;
    case TK_LIT_CHAR: print_nome (TK_LIT_CHAR); break;
    case TK_LIT_STRING: print_nome (TK_LIT_STRING); break;
    case TK_IDENTIFICADOR: print_nome (TK_IDENTIFICADOR); break;
    case TOKEN_ERRO:  print_nome (TOKEN_ERRO); break;
    default: printf ("<Invalid Token with code %d>\n", token); return 1; break;
    }
  }
  if (getenv("INF47_TABLE")){
    main_avaliacao_etapa_1_tabela();
  }
  // main_avaliacao_etapa_1_tabela();	// AQUIIIIIIIIIII!!!
  return 0;
}

void cc_dict_etapa_1_print_entrada (char *token, int line)
{
  printf("[%s] %d\n", token, line);
}

void cc_dict_etapa_2_print_entrada (char *token, int line, int tipo)
{
  printf("[%s] %d %d\n", token, line, tipo);
}

void main_avaliacao_etapa_1_tabela (void)
{
  comp_print_table();
}

int main_avaliacao_etapa_2 (int argc, char **argv)
{
 // printf("Aval2\n");
  gv_init(NULL);
  int ret = yyparse();
  gv_close();
  return ret;
}

int main_avaliacao_etapa_3 (int argc, char **argv)
{
  gv_init(NULL);
	//printf("A1\n");
  int ret = yyparse();
	//printf("A2\n");
  gv_close();
  return ret;
}

int main_avaliacao_etapa_4 (int argc, char **argv)
{
  return main_avaliacao_etapa_2 (argc, argv);
}

int main_avaliacao_etapa_5 (int argc, char **argv)
{
  return main_avaliacao_etapa_2 (argc, argv);
}

int main_avaliacao_etapa_6 (int argc, char **argv)
{
  return main_avaliacao_etapa_2 (argc, argv);
}

int main_avaliacao_etapa_7 (int argc, char **argv)
{
  return main_avaliacao_etapa_2 (argc, argv);
}

int calcula_pos_vetor(int dim,int* n, int* i){

	//printf("Dim:%d\n",dim);
	int d = 0;
	if(dim == 1){
		return i[dim-1];	
	}
	if(dim != 2){
		d = calcula_pos_vetor(dim-1,n,i)*n[dim-1]+i[dim-1];	
	}
	else {
		d = i[dim-2]*n[dim-1]+i[dim-1];
	}
	return d;
}
char* get_reg(){

	char *tipo = (char*)malloc(sizeof(char)*5);
	char *num = (char*)malloc(sizeof(char));
	strcpy(tipo, "r");
	sprintf(num, "%d", num_reg);
	strcat(tipo,num);	
	num_reg = num_reg + 1 ;
	return tipo;
}

char* get_last_reg(){

	char *tipo = (char*)malloc(sizeof(char)*5);
	char *num = (char*)malloc(sizeof(char));
	strcpy(tipo, "r");
	sprintf(num, "%d", num_reg-1);	
	strcat(tipo,num);
	return tipo;
}

char* get_last_1_reg(){

	char *tipo = (char*)malloc(sizeof(char)*5);
	char *num = (char*)malloc(sizeof(char));
	strcpy(tipo, "r");
	sprintf(num, "%d", num_reg-2);	
	strcat(tipo,num);
	return tipo;
}
char* get_last_2_reg(){

	char *tipo = (char*)malloc(sizeof(char)*5);
	char *num = (char*)malloc(sizeof(char));
	strcpy(tipo, "r");
	sprintf(num, "%d", num_reg-3);	
	strcat(tipo,num);
	return tipo;
}

char* get_label(){
	char *tipo = (char*)malloc(sizeof(char)*5);
	char *num = (char*)malloc(sizeof(char));
	strcpy(tipo, "L");
	sprintf(num, "%d", num_label);	
	strcat(tipo,num);	
	num_label = num_label + 1;
	return tipo;
} 

void tac_create( char* label, int opcode, char* op1, char* op2, char* op3){

	//printf("Entrei TAC create\n");
	tac_i *inst = malloc(sizeof(tac_i));
	
	inst -> label = label;	
	inst -> opcode = opcode; 
	inst ->  op1 = op1;
	inst ->  op2 = op2;
	inst ->  op3 = op3;
	inst->next = NULL;
	inst->prev = NULL;

	//printf("1 Lista: %d\n",lista);
	if(codigo == NULL ){ // 0 elelentos
		//printf("\tPrimeira TAC\n");
		codigo = inst;				
	}
	else{	//printf("\t+1 TAC\n");
		tac_i *aux = codigo;
		while(aux->next != NULL){ // pega ultimo elo
			aux = aux->next;	//printf("Percorre 1\n");
		}	
		inst->prev = aux;   // [  ]  <- [  ]
		aux->next = inst;	// [  ]  -> [  ]
		codigo = inst; 
	}
	//printf("Lista:%d\n",lista);

	//return lista;
}

char *tac_code_create(int tipocodigo,int operacao, int offset1, int offset2,char* c1, char* c2){

	//printf("Entrei tac_code_create\n");
	char *reg1 ;
	char *reg2 ;
	char *reg3 ;
	char *off1 = (char*)malloc(sizeof(char)*5);
	char *off2 = (char*)malloc(sizeof(char)*5);


	switch(tipocodigo){

	case sub_reg:// printf("Aritmetico reg reg\n");

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
//		tac_create(NULL,sub_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		tac_create(NULL,sub_opcode,c1,c2,reg1);			

		break;

	case arit_reg:// printf("Aritmetico reg reg\n");

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		//tac_create(NULL,add_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		tac_create(NULL,add_opcode,c1,c2,reg1);			
		break;

	case atribuicao_global:		
		sprintf(off1, "%d", offset1);
		//printf("Aqui Global\n");
		tac_create(NULL,storeAI_opcode,c1,"rbss",off1);// mem(rarp+offset) <= last_reg  		
		break;


	case atribuicao:// printf("Atribuição\n");

		//reg1 = get_last_reg(); // mudar
		//printf("R3 %s\n",reg1);
		sprintf(off1, "%d", offset1);
		//printf("Aqui\n");
		tac_create(NULL,storeAI_opcode,c1,"rarp",off1);// mem(rarp+offset) <= last_reg  		
		break;

	case mult_reg: //printf("Aritmetico reg reg\n");

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
//		tac_create(NULL,mult_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		tac_create(NULL,mult_opcode,c1,c2,reg1);			

		break;

	case div_reg:

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
//		tac_create(NULL,div_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		tac_create(NULL,div_opcode,c1,c2,reg1);			

		break;

	case lshift_reg: //printf("Left shift\n");

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,lshift_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case rshift_reg: //printf("Right Shift\n"); 

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,rshift_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case lcomp_reg: //a<b

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_LT_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case gcomp_reg: //a>b 

		//printf("\n\n\n\n**********************AQUI\n\n\n\n\n");
		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_GT_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case lecomp_reg: //a<=b

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_LE_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;	

	case gecomp_reg: //a>=b

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_GE_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case eqcomp_reg:

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_EQ_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case necomp_reg:

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,cmp_NE_opcode,get_last_2_reg(),get_last_1_reg(),reg1);			
		break;

	case or_reg: //a||b

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,or_opcode,get_last_2_reg(),get_last_1_reg(),reg1);
		break;

	case and_reg: //a||b

		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,and_opcode,get_last_2_reg(),get_last_1_reg(),reg1);
		break;

	case neg_reg:
		reg1 = get_reg();
		//printf("R1 %s\n",reg1);
		tac_create(NULL,and_opcode,NULL,get_last_1_reg(),reg1);
		break;
/*
	case arit_litvetc: printf("Literal vetor comutativo\n");

		break;
	case arit_litexpc:printf("Lit expre comutativo\n");
		reg1 = get_last_reg();
		reg2 = get_reg();
		printf("R1 %s\n",reg1);
		printf("R2 %s\n",reg2);

		tac_create(NULL,operacao,reg1,c1,reg2);	// r2 <= r1+c1
		
		break;
	case arit_litregc: // comutatiovo
		reg1 = get_reg();
		reg2 = get_reg();

	
		sprintf(off2, "%d", offset2);

		printf("R1 %s\n",reg1);
		printf("R2 %s\n",reg2);

		tac_create(NULL,loadAI_opcode,"rarp",off2,reg1);	// r1 <= rarp+offset1		
		tac_create(NULL,operacao,reg1,c1,reg2);	// r2 <= r1+c1

		break;

	case arit_imediato: printf("Aritmetico imediato\n"); 
			
			reg1 = get_reg();
			reg2 = get_reg();
			reg3 = get_reg();

			printf("R1 %s\n",reg1);
			printf("R2 %s\n",reg2);
			printf("R3 %s\n",reg3);
			
			printf("nome1%s\n",c1);
			printf("nome2%s\n",c2);
			 tac_create(NULL,loadI_opcode,c1,NULL,reg1);	// r1 <= c1
			 tac_create(NULL,loadI_opcode,c2,NULL,reg2);	// r2 <= c2
			 tac_create(NULL,operacao,reg1,reg2,reg3);	// r3 <= r1 + r2
	
			break;
*/
	}

	return reg1;
}

int search_key(char *key){
	int i = 0;
	content *achado;
	table_list *aux = tableList;
	do{
	achado = (content*)dict_get(aux->table, key);
	if(achado && achado->associated_type != -1){
		//printf("escopo: %d\n", i);
		return achado->associated_type;
	}
	aux = aux->next;
	i++;
	}while(aux != NULL);

	return -1;
}

void tac_ast(comp_tree_t* node){

	//printf("Entrei tac AST\n");
	comp_tree_t* filho_1;
	comp_tree_t* filho_2;
	int tipo_p;
	int tipo_f1;
	int tipo_f2;
	int offset1;
	int offset2;
	int offset_vetor;
	char *nome1;
	char *nome2;
	node_value* value_f1;
	node_value* value_f2;
	comp_dict_item_t* dict_f1;
	comp_dict_item_t* dict_f2;

	int global = -1;

	char *res_1;
	char *res_2;
	node_value* value_p = (node_value*)node->value;


	if(node->first != NULL){

		filho_1 = node->first;
	    value_f1 = (node_value*)filho_1->value;
		tipo_f1 = value_f1->node_type;
		dict_f1 = (comp_dict_item_t*)value_f1->SymbolTableEntry;
		res_1 = value_f1 -> reg_resultado;		
	
		if(node->first->next != NULL){
			
			filho_2 = node-> first->next; // verificar se é NULL (?)
		    value_f2 = (node_value*)filho_2->value;
		 	tipo_f2 = value_f2->node_type;	
			dict_f2 = (comp_dict_item_t*)value_f2->SymbolTableEntry;	
			res_2 = value_f2 -> reg_resultado;		

		}
	}

	if(dict_f1 != NULL){
		content* conteudo_f1 = dict_f1 ->value;
		//offset1 = conteudo_f1 -> offset_var;
		offset1=get_offset_var(dict_f1);	// Diferenciar se é global ??
	 	nome1 = (char*)malloc(sizeof(dict_f1->key)+1);
	 strcpy(nome1,dict_f1->key);
	}
	
	if(dict_f2 != NULL){
		content* conteudo_f2 = dict_f2 ->value;
//		offset2 = conteudo_f2 -> offset_var;
		offset2=get_offset_var(dict_f2);
	 	nome2 = (char*)malloc(sizeof(dict_f2->key)+1);
	 strcpy(nome2,dict_f2->key);

	}

 	tipo_p = value_p->node_type;

	//printf("****Node type P  :%d\n",tipo_p);
	//printf("****Node type F1 :%d\n",tipo_f1);
	//printf("****Node type F2 :%d\n",tipo_f2);
	
	//printf("**** Offset F1: %d\n",offset1);
	//printf("**** Offset F2: %d\n",offset2);

	switch(tipo_p) {
		// Caso especial para as operações não comutativas
		case AST_LOGICO_OU:
				value_p -> reg_resultado=tac_code_create(or_reg, or_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_E:
				value_p -> reg_resultado=tac_code_create(and_reg, and_opcode,-1,-1,NULL,NULL);
				break;
		case AST_ARIM_DIVISAO:
		value_p -> reg_resultado=tac_code_create(div_reg,div_opcode,-1,-1,res_1,res_2); 
				break;
		case AST_ARIM_MULTIPLICACAO:
		value_p -> reg_resultado=tac_code_create(mult_reg, mult_opcode,-1,-1,res_1,res_2);
				break;
		case AST_ARIM_SUBTRACAO:
				//printf("q\n");
		value_p -> reg_resultado=tac_code_create(sub_reg,sub_opcode,-1,-1,res_1,res_2); 
				break;
		case AST_ARIM_SOMA:

			value_p -> reg_resultado=tac_code_create(arit_reg,add_opcode,-1,-1,res_1,res_2); 
				break;
		case AST_ATRIBUICAO: 
			//if(tipo_f1 != AST_VETOR_INDEXADO )
			//value_p -> reg_resultado= tac_code_create(atribuicao,-1,offset1,-1,NULL,NULL);
			//else 
			//if(tipo_f2 != AST_VETOR_INDEXADO)	

			global = get_escopo_global(dict_f1);
			//printf("Sou uma atribuição global? %d\n",global);
			if(global==1)
		value_p -> reg_resultado= tac_code_create(atribuicao_global,-1,offset1,-1,res_2,NULL);
			else{	
		//printf("else\n");
		value_p -> reg_resultado= tac_code_create(atribuicao,-1,offset1,-1,res_2,NULL);

			}//else{
			//		a[i] = x[2];
			//}


				break;
		case AST_SHIFT_LEFT:
			value_p -> reg_resultado=tac_code_create(lshift_reg,lshift_opcode,-1,-1,NULL,nome2);
				break;
		case AST_SHIFT_RIGHT:
			value_p -> reg_resultado=tac_code_create(rshift_reg,rshift_opcode,-1,-1,NULL,nome2);
				break;
		case AST_LOGICO_COMP_L:
			value_p -> reg_resultado=tac_code_create(lcomp_reg,cmp_LT_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_G:
			value_p -> reg_resultado=tac_code_create(gcomp_reg,cmp_GT_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_LE:
			value_p -> reg_resultado=tac_code_create(lecomp_reg,cmp_LE_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_GE:
			value_p -> reg_resultado=tac_code_create(gecomp_reg,cmp_GE_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_IGUAL:
			value_p -> reg_resultado=tac_code_create(eqcomp_reg,cmp_EQ_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_DIF:
			value_p -> reg_resultado=tac_code_create(necomp_reg,cmp_NE_opcode,-1,-1,NULL,NULL);
				break;
		case AST_LOGICO_COMP_NEGACAO:
			value_p -> reg_resultado=tac_code_create(neg_reg,neg_opcode,-1,NULL,NULL,NULL);
				break;


	}
	
	//return inst; // ponteiro para a ultima instrução gerada
		//printf("Sai tac ast\n");
}

void imprime_codigo(){
	FILE *arq;
	arq = fopen("codigo.txt", "wr");
	if(arq == NULL){
			printf("Erro ao criar o arquivo\n");	
		}
	tac_i *aux = codigo;

	if(aux!=NULL)
		while(aux->prev != NULL){ // pega ultimo elo
			aux = aux->prev;	//printf("Percorre 1\n");
		}
	
	while(aux != NULL){ // pega ultimo elo
		
		switch(aux->opcode){

			case add_opcode: 
					if(aux->label==NULL)
						printf("	add  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					else
						printf("%s: add  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case sub_opcode: if(aux->label==NULL)
						printf("	sub  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					 else
						printf("%s: sub  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case mult_opcode: if(aux->label==NULL)
						printf("	mult  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					  else
						printf("%s: mult  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);
					 aux = aux->next;
					 break;

			case div_opcode: if(aux->label==NULL)
						printf("	div  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					 else
						printf("%s: div  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case addI_opcode: if(aux->label==NULL)
						printf("	addI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					  else
						printf("%s: addI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);					  

					aux = aux->next;
					  break;

			case subI_opcode: if(aux->label==NULL)
						printf("	subI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					  else
						printf("%s: subI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					  aux = aux->next;
					  break;

			case rsubI_opcode: if(aux->label==NULL)
						printf("	rsubI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					   else
						printf("%s: rsubI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);
					   aux = aux->next;
					   break;

			case multI_opcode: if(aux->label==NULL)
						printf("	multI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					   else
						printf("%s: multI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;
			case divI_opcode: if(aux->label==NULL)
						printf("	divI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					  else
						printf("%s: divI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case rdivI_opcode: if(aux->label==NULL)
						printf("	rdivI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					   else
						printf("%s: rdivI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case lshift_opcode: if(aux->label==NULL)
						printf("	lshift  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: lshift  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case lshiftI_opcode: if(aux->label==NULL)
						printf("	lshiftI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					     else
						printf("%s: lshiftI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case rshift_opcode: if(aux->label==NULL)
						printf("	rshift  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: rshift  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case rshiftI_opcode: if(aux->label==NULL)
						printf("	rshiftI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					     else
						printf("%s: rshiftI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case and_opcode: if(aux->label==NULL)
						printf("	and  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					 else
						printf("%s: and  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case andI_opcode: if(aux->label==NULL)
						printf("	andI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					  else
						printf("%s: andI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case or_opcode: if(aux->label==NULL)
						printf("	or  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					else
						printf("%s: or  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case orI_opcode: if(aux->label==NULL)
						printf("	orI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					else
						printf("%s: orI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case loadI_opcode: if(aux->label==NULL)
						printf("	loadI  %s => %s\n",aux->op1,aux->op3);			//op2 ou op3?
					   else
						printf("%s: loadI  %s => %s\n",aux->label,aux->op1,aux->op3);

					   aux = aux->next;
					   break;

			case load_opcode: if(aux->label==NULL)
						printf("	load  %s => %s\n",aux->op1,aux->op3);			//op2 ou op3?
					  else
						printf("%s: load  %s => %s\n",aux->label,aux->op1,aux->op3);

					   aux = aux->next;
					   break;

			case loadAI_opcode: if(aux->label==NULL)
						printf("	loadAI  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: loadAI  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);
					   aux = aux->next;
					   break;

			case loadAO_opcode: if(aux->label==NULL)
						printf("	loadAO  %s, %s => %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: loadAO  %s, %s => %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case store_opcode: if(aux->label==NULL)
						printf("	store  %s => %s\n",aux->op1,aux->op3);			//op2 ou op3?
					   else
						printf("%s: store  %s => %s\n",aux->label,aux->op1,aux->op3);

					   aux = aux->next;
					   break;

			case storeAI_opcode: if(aux->label==NULL)
						printf("	storeAI  %s => %s, %s\n",aux->op1,aux->op2,aux->op3);
					     else
						printf("%s: storeAI  %s => %s, %s\n",aux->label,aux->op1,aux->op2,aux->op3);
	
					   aux = aux->next;
					   break;

			case storeAO_opcode: if(aux->label==NULL)
						printf("	storeAO  %s => %s, %s\n",aux->op1,aux->op2,aux->op3);	
					     else
						printf("%s: storeAO  %s => %s, %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					   aux = aux->next;
					   break;

			case cmp_LT_opcode: if(aux->label==NULL)
						printf("	cmp_LT  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: cmp_LT  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cmp_GT_opcode: if(aux->label==NULL)
						printf("	cmp_GT  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: cmp_GT  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cmp_LE_opcode: if(aux->label==NULL)
						printf("	cmp_LE  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: cmp_LE  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cmp_GE_opcode: if(aux->label==NULL)
						printf("	cmp_GE  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: cmp_GE  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cmp_EQ_opcode: if(aux->label==NULL)
						printf("	cmp_EQ  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%s: cmp_EQ  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cmp_NE_opcode: if(aux->label==NULL)
						printf("	cmp_NE  %s, %s -> %s\n",aux->op1,aux->op2,aux->op3);
					    else
						printf("%d: cmp_NE  %s, %s -> %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case cbr: if(aux->label==NULL)
					printf("	cbr  %s -> %s, %s\n",aux->op1,aux->op2,aux->op3);
				  else
					printf("%s: cbr  %s -> %s, %s\n",aux->label,aux->op1,aux->op2,aux->op3);

					 aux = aux->next;
					 break;

			case jumpI_opcode: if(aux->label==NULL)
						printf("	jumpI  -> %s\n",aux->op1);			//op1 ou op2?
					   else
						printf("%s: jumpI  -> %s\n",aux->label,aux->op1);

					 aux = aux->next;
					 break;

			case nop_opcode: if(aux->label==NULL)
						printf("	nop\n");
					else
						printf("%s:\n", aux->label);

					 aux = aux->next;
					 break;
		}
	}	

}




//recebe uma AST que pode ser de qualquer tipo (soma, int, literal, vetor, multiplicacao, funcao, etc)
int check_type(comp_tree_t* element){
	int type1, type2;
	node_value* elementv = (node_value*)element->value;
	
	node_value* pai;
		
	//printf("%d\n",elementv->node_type);
	//nesse if, testa qual o tipo da AST -> somas, multiplicacoes, qualquer coisa com dois nodos
	//são tratados do mesmo jeito
	//somas, multiplicacoes, etc
	if( (elementv->node_type >= 12 && elementv->node_type <= 24) && elementv->node_type !=  16){
		//printf("Chamando type1\n");
		type1 = check_type(element->first);
		//printf("pr type1:%d\n",type1);
		//printf("Chamando type2\n");
		type2 = check_type(element->first->next);

		//printf("pr type2:%d\n",type2);
	}
	//literais & identificadores
	else if(elementv->node_type == 10 || elementv->node_type == 11){
		//printf("\nelementv type:%d\n",elementv->node_type);
		comp_dict_item_t *symboltable = (comp_dict_item_t *) elementv->SymbolTableEntry;	
		content *value = (content *) symboltable->value;
		if(value->type == 6){ //tratamento do caso de identificador
			type1 = search_key(symboltable->key); //procura se existe na tabela
			//printf("AQUIIII tipo asociado:%d\n",type1);

			if(type1==-1){ //caso nao exista
				printf(" * ERRO * Variavel nao declarada Linha:%d\n",value->line);
				exit(IKS_ERROR_UNDECLARED);
			}
			if(type1 >= 11 && type1 <= 15){ //caso seja do tipo funcao -> aqui é SÓ literal sozinho
				printf(" * ERRO * identificador %s deve ser usado como função Linha:%d\n",symboltable->key,value->line);
				exit(IKS_ERROR_FUNCTION);
			}
			return type1;	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Aqui modificado
			//falta aqui: caso seja do tipo vetor, também retorna erro
		}
		else {
				type1 = value->associated_type; //literal só retorna o tipo associado (int etc)
				elementv->inferred_type = type1;						
				//printf("\t\t\t\tentrei no else\n");
				return type1;
			}
	}
	//chamada de funcao - a chamada de funcao TEM um AST_IDENTIFICADOR mas ele NAO VAI NA RECURSAO
	else if(elementv->node_type == 27){

		pai = elementv;		// Aqui modificado

		comp_tree_t* check_type = element->first;
		elementv = (node_value*)check_type->value;
		comp_dict_item_t *symboltable = (comp_dict_item_t *) elementv->SymbolTableEntry;	
		content *value = (content *) symboltable->value;
		type1 = search_key(symboltable->key); //procura se existe na tabela
		//printf("aaaaatype1: %d\n",type1);
			if(type1==-1){ //caso nao exista
				printf(" * ERRO * Variavel nao declarada Linha:%d\n",value->line);
				exit(IKS_ERROR_UNDECLARED);
			}
			else if(type1==IKS_INT_FUNCTION){
						elementv->inferred_type = IKS_INT_VAR ;
						pai->inferred_type = IKS_INT_VAR ;
						return IKS_INT_VAR;	
					}  
			else if(type1==IKS_CHAR_FUNCTION){
						elementv->inferred_type = IKS_CHAR_VAR ;
						pai->inferred_type = IKS_CHAR_VAR ;
						return IKS_CHAR_VAR;
					} 
			else if(type1==IKS_STRING_FUNCTION){
						elementv->inferred_type = IKS_STRING_VAR;
						pai->inferred_type = IKS_STRING_VAR ;
						return IKS_STRING_VAR;
					} 
			else if(type1==IKS_FLOAT_FUNCTION){

						elementv->inferred_type = IKS_FLOAT_VAR;
						pai->inferred_type = IKS_FLOAT_VAR ;
						return IKS_FLOAT_VAR;
					} 
			else if(type1==IKS_BOOL_FUNCTION) {
						elementv->inferred_type = IKS_BOOL_VAR;
						pai->inferred_type = IKS_BOOL_VAR ;
						return IKS_BOOL_VAR;
					}
	}
	else if(elementv->node_type == 26){ 	// Caso seja um Vetor Indexado
		// Verificar numero de dimensões -> armazenar dimensoes na arvore ? 
		// Verificar no escopo global o tipo dele -> Feito
		//printf("Sou vetor indexado\n");

		pai = elementv;		// Aqui modificado

		comp_tree_t* check_type = element->first;
		elementv = (node_value*)check_type->value;
		comp_dict_item_t *symboltable = (comp_dict_item_t *) elementv->SymbolTableEntry;	
		content *value = (content *) symboltable->value;
		type1 = search_key(symboltable->key); //procura se existe na tabela
		//printf("aaaaatype1: %d\n",type1);
			if(type1==-1){ //caso nao exista
				printf(" * ERRO * Variavel nao declarada Linha:%d\n",value->line);
				exit(IKS_ERROR_UNDECLARED);
			}
			else if(type1==IKS_INT_VECTOR){
						elementv->inferred_type = IKS_INT_VAR ;
						pai->inferred_type = IKS_INT_VAR ;
						return IKS_INT_VAR;	
					}  
			else if(type1==IKS_CHAR_VECTOR){
						elementv->inferred_type = IKS_CHAR_VAR ;
						pai->inferred_type = IKS_CHAR_VAR ;
						return IKS_CHAR_VAR;
					} 
			else if(type1==IKS_STRING_VECTOR){
						elementv->inferred_type = IKS_STRING_VAR;
						pai->inferred_type = IKS_STRING_VAR ;
						return IKS_STRING_VAR;
					} 
			else if(type1==IKS_FLOAT_VECTOR){

						elementv->inferred_type = IKS_FLOAT_VAR;
						pai->inferred_type = IKS_FLOAT_VAR ;
						return IKS_FLOAT_VAR;
					} 
			else if(type1==IKS_BOOL_VECTOR) {
						elementv->inferred_type = IKS_BOOL_VAR;
						pai->inferred_type = IKS_BOOL_VAR ;
						return IKS_BOOL_VAR;
					}

//**************************************************************************************
// 	Não sei se é erro.  Declaramos um vetor global x e dentro do escopo declaramas uma variável com o mesmo nome 
//**************************************************************************************
		comp_dict_item_t *dict = (comp_dict_item_t *) elementv->SymbolTableEntry;
		while(dict == NULL){
			comp_tree_t* next = element->first;
			elementv = (node_value*)next->value;
			dict = (comp_dict_item_t *) elementv->SymbolTableEntry;
		}			
		content *valor = (content *) dict->value;
		 if(type1==IKS_INT_VAR){
	printf(" * ERRO * Redeclração de Variável Global. Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);
					}  
			else if(type1==IKS_CHAR_VAR){
	printf(" * ERRO * Redeclração de Variável Global. Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);
					} 
			else if(type1==IKS_STRING_VAR){
	printf(" * ERRO * Redeclração de Variável Global. Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);		
					} 
			else if(type1==IKS_FLOAT_VAR){
	printf(" * ERRO * Redeclração de Variável Global. Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);
					} 
			else if(type1==IKS_BOOL_VAR) {
	printf(" * ERRO * Redeclração de Variável Global. Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);
					}

//**************************************************************************************
	}
	//faltam nesse if: negacao, inversao, FUNCOES (CHECAR TIPOS DE ARGUMENTOS CONTRA OS DA TABELA!)
	//VETORES (tem que checar alguma coisa tambem, provavelmente se o retorno da exp é int?)
	//SEMPRE que for procurar na tabela tem que checar se ta fazendo compativel ex: nao chamando funcao sem parametro, vetor como se fosse funcao, etc.

	//RETORNOS POSSIVEIS
	if(type1 == IKS_INT_VAR && type2 == IKS_INT_VAR){ 
		elementv->inferred_type =IKS_INT_VAR ;
		 return IKS_INT_VAR;}
	else if(type1 == IKS_FLOAT_VAR && type2 == IKS_FLOAT_VAR) {
		elementv->inferred_type =IKS_FLOAT_VAR ;
		return IKS_FLOAT_VAR;}
	else if(type1 == IKS_BOOL_VAR  && type2 == IKS_BOOL_VAR ) {
		elementv->inferred_type =IKS_BOOL_VAR ;
			return IKS_BOOL_VAR;}
	else if(type1 == IKS_CHAR_VAR && type2 == IKS_CHAR_VAR) {
		elementv->inferred_type =IKS_CHAR_VAR ;
			return IKS_CHAR_VAR;}
	else if(type1 == IKS_STRING_VAR && type2 == IKS_STRING_VAR) {
		elementv->inferred_type =IKS_STRING_VAR ;
			return IKS_STRING_VAR;}
	else if((type1 == IKS_INT_VAR || type1 == IKS_BOOL_VAR) && type2 == IKS_FLOAT_VAR) {
		elementv->inferred_type = IKS_FLOAT_VAR;
			return IKS_FLOAT_VAR;}
	else if((type2 == IKS_INT_VAR || type2 == IKS_BOOL_VAR) && type1 == IKS_FLOAT_VAR) {
		elementv->inferred_type = IKS_FLOAT_VAR;
			return IKS_FLOAT_VAR;}
	else if(type1 == IKS_INT_VAR && type2 == IKS_BOOL_VAR) {
		elementv->inferred_type =IKS_INT_VAR ;
			return IKS_INT_VAR;}
	else if(type2 == IKS_INT_VAR && type1 == IKS_BOOL_VAR) {
		elementv->inferred_type =IKS_INT_VAR ;
			return IKS_INT_VAR;}
	else { //TIPOS DE ARGUMENTOS INVALIDOS JUNTOS RETORNAM ERRO
		comp_dict_item_t *dict = (comp_dict_item_t *) elementv->SymbolTableEntry;
		while(dict == NULL){
			comp_tree_t* next = element->first;
			elementv = (node_value*)next->value;
			dict = (comp_dict_item_t *) elementv->SymbolTableEntry;
		}			
		content *valor = (content *) dict->value;
		if(type1==IKS_STRING_VAR){
printf(" * ERRO * Coercao impossivel do tipo string Linha:%d\n",valor->line);
		exit(IKS_ERROR_STRING_TO_X);
		}
		else if(type1==IKS_CHAR_VAR){
			printf(" * ERRO * Coercao impossivel do tipo char Linha:%d\n",valor->line);
			exit(IKS_ERROR_CHAR_TO_X);
		}
		else if(type2==IKS_STRING_VAR){
printf(" * ERRO * Coercao impossivel do tipo string Linha:%d\n",valor->line);
		exit(IKS_ERROR_STRING_TO_X);
		}
		else if(type2==IKS_CHAR_VAR){
			printf(" * ERRO * Coercao impossivel do tipo char Linha:%d\n",valor->line);
			exit(IKS_ERROR_CHAR_TO_X);
		}

		//printf("************* Pre erro chechtype\n");
		//printf("Type1:%d Type2:%d\n",type1,type2);
		//printf(" * ERRO * Tipos incompatíveis Linha:%d\n",valor->line);
		exit(IKS_ERROR_WRONG_TYPE);
	}
}
int get_element_type_size(comp_dict_item_t * id){

	//printf("\nEntrei get element size\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return achado->type_size;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}
int get_dimensoes(comp_dict_item_t * id){

	//printf("\nEntrei get dimensoes\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return achado->dimensoes;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}
dim_list * get_dim_list(comp_dict_item_t * id){

	//printf("\nEntrei get offset_var\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{	
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		///printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return achado->dim_list;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}

int get_escopo_global(comp_dict_item_t * id){

	//printf("\nEntrei get offset_var\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d gl%d\n",achado->line,achado->escopo_global );
			return achado->escopo_global;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);


	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}

int get_offset_var(comp_dict_item_t * id){

	//printf("\nEntrei get offset_var\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
		//	printf("\tAchei %d\n",achado->line);
			return achado->offset_var;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}


int declaration_verification(comp_dict_item_t * id){

	//printf("\nentrei declaration verification\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return achado->associated_type;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}


int declaration_verification_same_scope(comp_dict_item_t * id){

	//printf("\nentrei declaration verification\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return achado->associated_type;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);


	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}

int string_type_verification(comp_tree_t* id){

	//printf("String type verification\n");
	comp_dict_item_t *entrada ;
	content *achado ;
	comp_tree_t *arvore = id;

	node_value *valor = id->value;
	
	//printf("Tipo de nodo:%d\n",valor->node_type);

	entrada = valor->SymbolTableEntry;
	
	if(valor->node_type == 11 ){

		if(entrada!= NULL){
			//printf("\tEXPVER Sou %s\n",entrada->key);
			content *conteudo = (content *) entrada->value;
			//printf("\t\t\t\t %d\n",conteudo->associated_type);						
	
		
		if(conteudo->associated_type == 4 ){
			//printf("É uma string\n");
				return 1;
			}
			else 
				if(conteudo->associated_type == 9 ){
					//printf("É uma string\n");
					return 1;
				}
				else 
					if(conteudo->associated_type == 14 ){
						//printf("É uma string\n");
						return 1;
					}			
					else
						return -1;		

			}

	}
	else {
		//printf("Não é um nodo literal logo não poderia nem ser uma string\n");
		return -1;
	}
}


int expression_verification(comp_tree_t* id, int tipo2){

	comp_dict_item_t *entrada ;
	content *achado ;
	comp_tree_t *arvore = id;

	node_value *valor = id->value;
	
	//printf("Tipo de nodo:%d\n",valor->node_type);

	entrada = valor->SymbolTableEntry;

	if(entrada!= NULL)
		//printf("\tEXPVER Sou %s\n",entrada->key);
	
	if(valor->node_type == tipo2){
		//printf("Tipos iguais\n");
		return 	1;
	}
	else {
		//printf("Tipos diferentes\n");
		return -1;
	}
}

int tree_line(comp_tree_t * id){
	//printf("Entrei tree line\n");
	content *achado ;
	comp_dict_item_t *entrada ;

	comp_tree_t *arvore = id;
	node_value *valor = id->value;

	table_list *aux = tableList;

	entrada = (comp_dict_item_t *) valor->SymbolTableEntry;

	if(entrada == NULL){
		//printf("Entrada é NULL\n");
		tree_line(id->first);		
	}
	else {


	
	//content *valores = (content *) entrada->value;

	
do{
	 achado =(content *) dict_get(aux->table,entrada->key);

	if(achado){
		//printf("-------------------------------------------Encontrei retornarei a linha\n");
		return achado->line;	
	}

	aux = aux->next;
	}
	while(aux != NULL);
}

}


int get_line(comp_dict_item_t * id){

	//printf("Entrei get line\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	 achado =(content *) dict_get(aux->table,entrada->key);

	if(achado){
		//printf("-------------------------------------------Encontrei retornarei a linha\n");
		return achado->line;	
	}

	aux = aux->next;
	}
	while(aux != NULL);
}

table_list * insert_first(comp_dict_t *Table){
	//printf("Insert First\n");
	table_list *newitem = malloc(sizeof(table_list));

	if(tableList==NULL){
		//printf("Pilha Vazia\n");
		newitem->table = Table;
		newitem->next = NULL;
		return newitem;
	}
	else{
		//printf("Pelo menos 1 elemento\n");
		newitem->table = Table;
		newitem->next = tableList;
		return newitem;
	}
}


void init_scope(){
	//tableList = NULL;
	//comp_dict_t *SymbolTable;

	SymbolTable = dict_new();

    tableList = insert_first(SymbolTable);
	///printf("Id novo escopo %d\n",tableList);
	//printf("Id tabela simbolos: %d\n",SymbolTable);
}

void end_scope(){ // elimina o primeiro escopo
 	//remove_first(tableList);
	//printf("entrei end scope\n");
	//printf("1 Id tabela simbolos: %d\n",SymbolTable);
	//printf("1\t\t %d\n",tableList);

	//printf("tabela end:%d\n",tableList->table);	
	
	dict_free(tableList->table);

	//printf("Sai dict free\n");

	table_list* temp = tableList;
	tableList = tableList->next;
	//free(temp->next);
	free(temp);
	if(tableList != NULL)
		SymbolTable = tableList->table;

	//printf("2\t\t %d\n",tableList);
	//printf("2Id tabela simbolos: %d\n",SymbolTable);
}

int declaration_verification_type(comp_dict_item_t * id){

	//printf("\nentrei declaration verification\n");
	table_list *aux = tableList;
	comp_dict_item_t *entrada ;
	content *achado ;

	entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	
do{
	//printf("1ID AUX: %d\n",aux);
	 achado =(content *) dict_get(aux->table,entrada->key);

	//printf("prev\n");
	if(achado){
		//printf("\t\tRetornei Dict_Get %d\n",achado->line);
		if(achado->associated_type != -1){
			//printf("\tAchei %d\n",achado->line);
			return 	achado->associated_type;
		//	return 1;	
		}
	}

	aux = aux->next;
	//printf("2ID AUX: %d\n",aux);

	}
	while(aux != NULL);

	printf(" * ERRO * Variável não declarada Linha:%d\n",valor->line);
	exit(IKS_ERROR_UNDECLARED);

}

// Verifica dentro do escopo se foi declarada a variável .
int set_associated_type(comp_dict_item_t * id, int tipo){

	table_list *auxScope = tableList;

	//printf("\n******** ESTOU AQUI********* \n");

	//printf("-----------------Entrei set\n");
	comp_dict_item_t *entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	if(valor->associated_type == -1){	// Declarando pela primeira vez -> Certo
		valor->associated_type = tipo;
		//printf("\t\t\tPrimera vez\n");

		switch(tipo){
			case IKS_INT_VAR: valor->element_size = INT_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);				
					//printf("\n\n\n******Identificador: %s, %d", valor->data.string_value, valor->element_size);
					break;
			case IKS_FLOAT_VAR: valor->element_size = FLOAT_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					//printf("\n\n\n******Identificador: %s, %d", valor->data.string_value, valor->element_size);
					break;
			case IKS_CHAR_VAR: valor->element_size = CHAR_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			case IKS_BOOL_VAR: valor->element_size = BOOL_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			//REVEEEEEEEEEEER STRING
			case IKS_STRING_VAR: valor->element_size = CHAR_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			default: valor->element_size=0;

		}

			return 1;
	}else 
		if(valor->associated_type != -1 ){	// Redeclaração -> erro
			printf(" * ERRO * Redeclaração de Variável Linha:%d\n",valor->line);
			exit(IKS_ERROR_DECLARED);		
		}

}
int set_associated_type0(comp_dict_item_t * id, int tipo){

	table_list *auxScope = tableList;

	dim_list* aux = diml;

	//printf("-----------------Entrei set\n");
	comp_dict_item_t *entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;

	//printf("\n\n---------SET ASSOCIATED TYPE 2-----\n");
	int n =0;
	int tamanho = 1;

	if(valor->associated_type == -1){	// Declarando pela primeira vez -> Certo
		valor->associated_type = tipo;
		valor -> dim_list = diml;
		valor -> escopo_global = -1;	// Etapa 5
		

				switch(tipo){
					case IKS_INT_VECTOR:
							valor -> type_size = 4;
						break;
					case IKS_FLOAT_VECTOR:
							valor -> type_size = 8;
						break;
					case IKS_BOOL_VECTOR:
							valor -> type_size = 1;
						break;
					case IKS_CHAR_VECTOR:
							valor -> type_size = 1; 
						break;
					case IKS_STRING_VECTOR:
							valor -> type_size = 1;// Não sei
						break;
					case IKS_USER_TYPE_VECTOR:
							valor -> type_size = 1;// Não sei
						break;					
				}

	
		while(aux!= NULL){
			tamanho = tamanho * aux->dim;			
			n = n + 1;		
			aux = aux->next;
		}
	//printf("Aqui\n");
		valor -> dimensoes = n;
	//	printf("Dimensoes: %d\n",n);
	//	printf("Tamanho vetor:%d\n",tamanho);
		//printf("\t\t\tPrimera vez\n");
			
		switch(tipo){

			case IKS_INT_VECTOR:
			valor->element_size = INT_SIZE * tamanho;
			valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
			auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; 
					break;
			case IKS_FLOAT_VECTOR:
					valor->element_size = FLOAT_SIZE *tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
				
					break;
			case IKS_BOOL_VECTOR:
		valor->element_size = BOOL_SIZE * tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
				
					break;
			case IKS_CHAR_VECTOR:
	valor->element_size = CHAR_SIZE * tamanho;
	valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
	auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
			
					break;
			case IKS_STRING_VECTOR:
		valor->element_size = CHAR_SIZE * tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
		
					break;
}

		return 1;

	}else 
		if(valor->associated_type != -1 ){	// Redeclaração -> erro
			printf(" * ERRO * Redeclaração de Variável Linha:%d\n",valor->line);
			exit(IKS_ERROR_DECLARED);		
		}

}
// Verifica dentro do escopo se foi declarada a variável .
int set_associated_type2(comp_dict_item_t * id, int tipo){

	table_list *auxScope = tableList;

	dim_list* aux = diml;

	//printf("-----------------Entrei set\n");
	comp_dict_item_t *entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;

	//printf("\n\n---------SET ASSOCIATED TYPE 2-----\n");
	int n =0;
	int tamanho = 1;

	if(valor->associated_type == -1){	// Declarando pela primeira vez -> Certo
		valor->associated_type = tipo;
		valor -> dim_list = diml;
		valor -> escopo_global = 1;	// Etapa 5
		

				switch(tipo){
					case IKS_INT_VECTOR:
							valor -> type_size = 4;
						break;
					case IKS_FLOAT_VECTOR:
							valor -> type_size = 8;
						break;
					case IKS_BOOL_VECTOR:
							valor -> type_size = 1;
						break;
					case IKS_CHAR_VECTOR:
							valor -> type_size = 1; 
						break;
					case IKS_STRING_VECTOR:
							valor -> type_size = 1;// Não sei
						break;
					case IKS_USER_TYPE_VECTOR:
							valor -> type_size = 1;// Não sei
						break;					
				}

	
		while(aux!= NULL){
			tamanho = tamanho * aux->dim;			
			n = n + 1;		
			aux = aux->next;
		}
	//printf("Aqui\n");
		valor -> dimensoes = n;
	//	printf("Dimensoes: %d\n",n);
	//	printf("Tamanho vetor:%d\n",tamanho);
		//printf("\t\t\tPrimera vez\n");
			
		switch(tipo){

			case IKS_INT_VECTOR:
			valor->element_size = INT_SIZE * tamanho;
			valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
			auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; 
					break;
			case IKS_FLOAT_VECTOR:
					valor->element_size = FLOAT_SIZE *tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
				
					break;
			case IKS_BOOL_VECTOR:
		valor->element_size = BOOL_SIZE * tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
				
					break;
			case IKS_CHAR_VECTOR:
	valor->element_size = CHAR_SIZE * tamanho;
	valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
	auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
			
					break;
			case IKS_STRING_VECTOR:
		valor->element_size = CHAR_SIZE * tamanho;
		valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
		auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
		
					break;
}

		return 1;

	}else 
		if(valor->associated_type != -1 ){	// Redeclaração -> erro
			printf(" * ERRO * Redeclaração de Variável Linha:%d\n",valor->line);
			exit(IKS_ERROR_DECLARED);		
		}

}
int set_associated_type3(comp_dict_item_t * id, int tipo){

	table_list *auxScope = tableList;

	//printf("\n******** ESTOU AQUI********* \n");

	//printf("-----------------Entrei set\n");
	comp_dict_item_t *entrada = (comp_dict_item_t *) id;	
	content *valor = (content *) entrada->value;
	if(valor->associated_type == -1){	// Declarando pela primeira vez -> Certo
		valor->associated_type = tipo;
		valor -> escopo_global = 1;	// Etapa 5
			//printf("\t\t\tPrimera vez\n");

		switch(tipo){
			case IKS_INT_VAR: valor->element_size = INT_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);				
					//printf("\n\n\n******Identificador: %s, %d", valor->data.string_value, valor->element_size);
					break;
			case IKS_FLOAT_VAR: valor->element_size = FLOAT_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					//printf("\n\n\n******Identificador: %s, %d", valor->data.string_value, valor->element_size);
					break;
			case IKS_CHAR_VAR: valor->element_size = CHAR_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			case IKS_BOOL_VAR: valor->element_size = BOOL_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			//REVEEEEEEEEEEER STRING
			case IKS_STRING_VAR: valor->element_size = CHAR_SIZE;
					valor->offset_var = auxScope->table->offset_dict;	//ATUALIZA POSIÇÃO DA VARIAVEL
					auxScope->table->offset_dict = auxScope->table->offset_dict + valor->element_size; //ATUALIZA OFFSET DO ESCOPO
					//printf("Posicao da variavel: %d\n", valor->offset_var);
					//printf("Offset atual do escopo: %d\n", auxScope->table->offset_dict);
					break;
			default: valor->element_size=0;

		}

			return 1;
	}else 
		if(valor->associated_type != -1 ){	// Redeclaração -> erro
			printf(" * ERRO * Redeclaração de Variável Linha:%d\n",valor->line);
			exit(IKS_ERROR_DECLARED);		
		}

}

int main (int argc, char **argv)
{	//printf("Entrei main\n");
	tableList = NULL;
 	num_label = 0;	// Reservar algum registrador ??
 	num_reg = 0;	
   // SymbolTable = dict_new();
    //init_scope();
	//root = tree_make_node(NULL);		// Cria o nodo raiz para o programa

  //if some argument is provided, treat it as input
  if (argc != 1){
    yyin = fopen(argv[1], "r");
    //if fopen fails, yyin continues to be stdin
    if (yyin == NULL){
      yyin = stdin;
    }
  }

  USER_INIT;
  int r;
#ifdef AVALIACAO_ETAPA_1
  r = main_avaliacao_etapa_1 (argc, argv);
#elif AVALIACAO_ETAPA_2
   r = main_avaliacao_etapa_2 (argc, argv);
#elif AVALIACAO_ETAPA_3
 	printf("Aqui\n"); r = main_avaliacao_etapa_3 (argc, argv);
#elif AVALIACAO_ETAPA_4
  r = main_avaliacao_etapa_4 (argc, argv); 
#elif AVALIACAO_ETAPA_5
  r = main_avaliacao_etapa_5 (argc, argv);
#elif AVALIACAO_ETAPA_6
  r = main_avaliacao_etapa_6 (argc, argv);
#elif AVALIACAO_ETAPA_7
  r = main_avaliacao_etapa_7 (argc, argv);
#else
  r = 0;
#endif

  USER_FINALIZE;
  return r;
}
