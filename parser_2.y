/*
  Coloque aqui o identificador do grupo e dos seus membros
*/
%{
#include <stdio.h>
#include "../include/cc_dict.h"
#include "../include/cc_tree.h"
#include "../include/cc_ast.h"
extern comp_tree_t* root;			// Raiz da arvore (usar ?)

%}

%union {	

	struct  comp_dict_item_t *valor_simbolo_lexico;
	struct  comp_tree_t *astTree;		/// Aqui modificado
}
%start program
/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR

%token <valor_simbolo_lexico> TK_LIT_INT	"Numero"
%token <valor_simbolo_lexico> TK_LIT_FLOAT
%token <valor_simbolo_lexico> TK_LIT_FALSE	
%token <valor_simbolo_lexico> TK_LIT_TRUE	
%token <valor_simbolo_lexico> TK_LIT_CHAR
%token <valor_simbolo_lexico> TK_LIT_STRING
%token <valor_simbolo_lexico> TK_IDENTIFICADOR "Identificador"

%token TOKEN_ERRO


//------------------------------Etapa 3 -----------------------------------------------
// Devemos colocar o tipo para todos os não terminais (Pagina 70 manual bison) 
// O professor disse no email que não tinha que ser asssim .... mas.... 
%type <astTree> program
%type <astTree> decList "decList"
%type <astTree> dec 
%type <astTree> input
%type <astTree> output
%type <astTree> outputList
%type <astTree> ifdec 
%type <astTree> simplecommandcall
%type <astTree> return
%type <astTree> whiledec
%type <astTree> dowhiledec
//%type <astTree> globalvardec
//%type <astTree> usertypedec 
%type <astTree> funcdec
%type <astTree> simplebody
%type <astTree> commandblockdec
%type <astTree> simplecommand
%type <astTree> expression
%type <astTree> attrdec 
%type <astTree> primitiveTypeAttr
//%type <astTree> shiftdec
%type <astTree> exp2
//------------------------------------------------------------------------------------

%left TK_OC_AND TK_OC_OR
%left  TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
%left '<' '>' '!'
%left '-' '+'
%left '*' '/' 
%left UMINUS

%nonassoc TK_PR_THEN PR
%nonassoc TK_PR_ELSE 


%precedence TK_PR_OUTPUT
%precedence '}'
%precedence ';'
%precedence TK_IDENTIFICADOR
%precedence '('
%precedence ','



%%
//-----------------------------------------------------------------------------------------
//PROGRAMA
// Program do tipo astTree ou extern de root: root = $1 ???
program:    decList { root = $1; };	 // equivalente a dizer program = declist 

decList:     decList dec {	printf("Entrei dec\n");
							node_value *valor = malloc(sizeof(node_value));
							valor->node_type=AST_PROGRAMA  ;
							valor->SymbolTableEntry = NULL;
							$$ = tree_make_unary_node((void*)valor,$2); //$2 deve ser comp_tree_t
						 }
	        |/*nothing*/ {	$$ =NULL;} 						//	   /\
	        ;												//	   |	
															//	   |		
dec:   funcdec { $$ = $1;} // Passando o parâmetro para cima //    |
		 //globalvardec 
        //| usertypedec 
		//| funcdec
	        ; 

//----------------------------------------------------------------------------------------
//STATIC
staticprec:	TK_PR_STATIC
		|//nothing
		; 

types:          prType 
		| userType
		;

prType:         TK_PR_INT
		|TK_PR_FLOAT
		|TK_PR_BOOL 		
		|TK_PR_CHAR
		|TK_PR_STRING		
	        ;	

userType:       TK_IDENTIFICADOR ;
/*
//-----------------------------------------------------------------------------------------
//DECLARACAO DE VARIAVEL GLOBAL
globalvardec:   staticprec types TK_IDENTIFICADOR varVector ';';

varVector:      '['TK_LIT_INT']' // Como garantir que é positivo ? Novo tipo Natural ?
 		|//nothing
		;
//-----------------------------------------------------------------------------------------
//TIPO DO USUARIO - a lista de parametros nao pode ser vazia
usertypedec:    TK_PR_CLASS TK_IDENTIFICADOR '['fieldList']' ';'; 

fieldList:	fieldList field 		
 		| fieldList ':'
		|//nothing 
		;

field:          fieldEncap fieldType fieldId ';';  
                                        
fieldEncap:	TK_PR_PROTECTED
		|TK_PR_PRIVATE
		|TK_PR_PUBLIC	
		;

fieldType:      prType ;

fieldId:	userType ;
*/
//-----------------------------------------------------------------------------------------
//DECLARACAO DE FUNCAO
funcdec:        header simplebody { printf("Enrei função\n");
									node_value *valor = malloc(sizeof(node_value));
									valor->node_type=AST_FUNCAO  ;
									valor->SymbolTableEntry = NULL; // como atribuir ptr tabela de simbolos
									$$ = tree_make_unary_node((void*)valor,$2);
									}
				; 

header:         staticprec types TK_IDENTIFICADOR '(' headerparam ')' ; 

headerparam:    param
		| headerparam ',' param
		|//nothing
		;

param:	        paramprec paramType paramName ;

paramprec:      TK_PR_CONST
		|//nothing
		;

paramType:      prType
		|userType
		;

paramName:      TK_IDENTIFICADOR ;

/*
body:           '{' commandblockdec  pv 
                | '{' ';' pv
                ; 
                
                

pv:             '}'';'
                |'}'
                ;
*/
simplebody: '{' commandblockdec '}'{ $$ = $2 ;} // simplebody = commandblockdec
			;
//-----------------------------------------------------------------------------------------
//BLOCOS DE COMANDO
commandblockdec:  commandblockdec simplecommand { $$=$2 ;} // filhos devem ter tipo comando correspondente
                  |/*nothing*/ {$$=NULL;} 
		  ;

simplecommand: // attrdec ';' {$$=$1;}; {      node_value *valor = malloc(sizeof(node_value));
									//valor->node_type=AST_ATRIBUICAO   ;
									//valor->SymbolTableEntry = NULL;
									//$$ = tree_make_unary_node((void*)valor,$1); 
							//};

				// locvardec ';'
                | attrdec ';'{$$=$1;}
	     		| input ';'{$$=$1;}
	     		| output ';'{$$=$1;}
      	        //| funcalldec ';'
      	        //| shiftdec ';' {$$=$1;}
	     		//| return ';'
	      		//| break ';'
	      		//| continue ';'
				//| body	
      	        | ifdec ';' {$$=$1;}
      	        | whiledec ';' {$$=$1;}
                | dowhiledec ';'{$$=$1;}
                //| fordec ';'
                //| foreachdec ';'
                //| switchdec ';'
                //| casedec
		        ;


simplecommandcall: 	//locvardec 
          			| attrdec {$$=$1;} 
     	 			| input {$$=$1;}
     				| output {$$=$1;}
					//| funcalldec 
           			//| shiftdec {$$=$1;}	
    	 			//| return 
      				//| break 		
      				//| continue 		
					//| simplebody			             	
					| ifdec {$$=$1;}
                    | whiledec  {$$=$1;}
                    | dowhiledec {$$=$1;}
                    //| fordec 
                    //| foreachdec 
                    //| switchdec 
                    //| casedec

              ;

//-----------------------------------------------------------------------------------------
// DECLARACAO DE VARIAVEL LOCAL -Verificar atribuição para usertype que deve estar proibida
/*
locvardec:      locType TK_IDENTIFICADOR varInit ;

locType:        TK_PR_STATIC userType
                | TK_PR_STATIC TK_PR_CONST userType
                | TK_PR_CONST userType
                | userType
                | TK_PR_STATIC prType
                | TK_PR_CONST prType
                | TK_PR_STATIC TK_PR_CONST prType
                | prType
                ;

varInit:        TK_OC_LE value 
		| //nothing
		;
                
value:	        TK_LIT_INT
		|TK_LIT_FLOAT
		|TK_LIT_FALSE
		|TK_LIT_TRUE
		|TK_LIT_CHAR
		|TK_LIT_STRING
		|TK_IDENTIFICADOR
		; 
*/ 
//----------------------------------------------------------------------------------------
// EXPRESSOES ARITMETICAS
expression:		TK_LIT_INT {
							printf("Literal Int\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
						}
                | TK_LIT_FLOAT {
							printf("Literal Float\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
							} 
                | TK_LIT_CHAR{
							printf("Literal Char\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
							}
				| TK_LIT_STRING{
							printf("Literal String\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
							}
                | TK_LIT_TRUE{
							printf("Literal True\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
							}
                | TK_LIT_FALSE{
							printf("Literal False\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_LITERAL  ;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));

							arvore = tree_make_node((void*) value_ID);
							//$$ = value_ID;
							$$ = arvore;
							}

		| expression '*' expression {   printf("Expresion\n");
							node_value *value = malloc(sizeof(node_value));
                        				value->node_type = AST_ARIM_MULTIPLICACAO;
				value->SymbolTableEntry = NULL; // como passar ponteiro para a tabela de simbolos
							$$ = tree_make_binary_node((void*)value,$1,$3);
                                             }
                | expression '-' expression {
							//nós subtraçao
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_ARIM_SUBTRACAO;
							value->SymbolTableEntry = NULL; 					

							//coloca as expressions como nós
							$$ = tree_make_binary_node((void*)value,$1,$3);

						}
                | expression '+' expression {
							//nós soma
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_ARIM_SOMA;
							value->SymbolTableEntry = NULL; 					

							//coloca as expressions como nós
							$$ = tree_make_binary_node((void*)value,$1,$3);

						}
                | expression '/' expression {
							//nós soma
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_ARIM_DIVISAO;
							value->SymbolTableEntry = NULL; 					

							//coloca as expressions como nós
							$$ = tree_make_binary_node((void*)value,$1,$3);

						}
                | expression '>' expression     {       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_G;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression '<' expression     {      
							 node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_L;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | '-' expression                {       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_ARIM_INVERSAO;
		value->SymbolTableEntry = NULL; //COLOCAR NA TABELA DE SIMBOLOS OU NAO?					
							$$ = tree_make_unary_node((void*)value,$2);    
                                                }
                | '+' expression               //nao existe nenhuma regras pra isso no ast.h
                | expression TK_OC_LE expression{       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_LE;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression TK_OC_GE expression{       
						node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_GE;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression TK_OC_EQ expression{       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_IGUAL;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression TK_OC_NE expression{      
 							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_COMP_DIF;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression TK_OC_AND expression{       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_E;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
                | expression TK_OC_OR expression{       
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_LOGICO_OU;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$1,$3);    
                                                }
				| TK_IDENTIFICADOR '['expression']'{ // Verificar
 							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_VETOR_INDEXADO ;
							value->SymbolTableEntry = NULL; 	

							printf("Identificador de vetor indexado\n");
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_IDENTIFICADOR;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
							arvore = tree_make_node((void*) value_ID);
				
							$$ = tree_make_binary_node((void*)value,arvore,$3);    

									}                        
                | exp2
		;

exp2:   TK_IDENTIFICADOR { printf("Identificador\n");
							node_value *value_ID = malloc(sizeof(node_value));
                                                        value_ID->node_type = AST_IDENTIFICADOR;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
							arvore = tree_make_node((void*) value_ID);
							$$ = arvore;
							//$$ = value_ID;
							}
		//'(' expression ')' 
//        |funcalldec
        //|TK_IDENTIFICADOR

        ;

   
//-----------------------------------------------------------------------------------------
// ATRIBUICAO
attrdec:    primitiveTypeAttr {$$=$1;}
	     //   |userTypeAttr {$$=$1;}
		;

primitiveTypeAttr:      TK_IDENTIFICADOR '=' expression {

								printf("primitiveattr\n");
								node_value *value = malloc(sizeof(node_value));
                                value->node_type = AST_ATRIBUICAO;
								value->SymbolTableEntry = NULL; 

// criar nodo ID
							node_value *value_ID = malloc(sizeof(node_value));
                            value_ID->node_type = AST_IDENTIFICADOR;
							value_ID->SymbolTableEntry = NULL; // como apontar ??

							comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
							arvore = tree_make_node((void*) value_ID);
						
							$$ = tree_make_binary_node((void*)value,arvore,$3);
							}
		       // | TK_IDENTIFICADOR '['expression']' '=' expression
		        ;

//userTypeAttr:   TK_IDENTIFICADOR '!' TK_IDENTIFICADOR '=' expression ;  

//-----------------------------------------------------------------------------------------
//INPUT
input:          TK_PR_INPUT expression {               
							 node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_INPUT;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_unary_node((void*)value,$2);    
                                       };

//-----------------------------------------------------------------------------------------
//OUTPUT
output:         TK_PR_OUTPUT outputList {               
						node_value *value = malloc(sizeof(node_value));
						value->node_type = AST_OUTPUT;
						value->SymbolTableEntry = NULL; 					
						$$ = tree_make_unary_node((void*)value,$2);    
                                       };

outputList:     outputList expression {               
						node_value *value = malloc(sizeof(node_value));
						value->node_type = AST_OUTPUT;
						value->SymbolTableEntry = NULL; 					
						$$ = tree_make_unary_node((void*)value,$2);    
                                       };
	        | outputList ','
		|//nothing
		;
/*
//-----------------------------------------------------------------------------------------
// FUNCTION CALL
funcalldec:     TK_IDENTIFICADOR '('funcallarg')' ;

funcallarg:     funcallarg expression
		| funcallarg ','
		| // nothing
		;

//-----------------------------------------------------------------------------------------
// SHIFT
shiftdec:       TK_IDENTIFICADOR shiftsym shiftvalue ;

shiftsym:       TK_OC_SR {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_RIGHT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
            			value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = NULL; // como apontar ??
				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
			
				$$ = tree_make_binary_node((void*)value,arvore,$3);    
                       }
                | TK_OC_SL {               
				node_value *value = malloc(sizeof(node_value));
				value->node_type = AST_SHIFT_LEFT;
				value->SymbolTableEntry = NULL;

				node_value *value_ID = malloc(sizeof(node_value));
            			value_ID->node_type = AST_IDENTIFICADOR;
				value_ID->SymbolTableEntry = NULL; // como apontar ??
				comp_tree_t *arvore = malloc(sizeof(comp_tree_t));
				arvore = tree_make_node((void*) value_ID);
			
				$$ = tree_make_binary_node((void*)value,arvore,$3);    
                       }
                ;

shiftvalue:     TK_LIT_INT ; 
*/
//-----------------------------------------------------------------------------------------
//RETURN
return:        TK_PR_RETURN expression {        
							 node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_RETURN;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_unary_node((void*)value,$2);    
                       };
/*
//-----------------------------------------------------------------------------------------
//BREAK
break:          TK_PR_BREAK ;
//-----------------------------------------------------------------------------------------
//CONTINUE
continue:       TK_PR_CONTINUE ;
//-----------------------------------------------------------------------------------------
//CASE
casedec:       TK_PR_CASE TK_PR_INT ':';
*/
//-----------------------------------------------------------------------------------------
// IF
ifdec:          TK_PR_IF '('expression')'  TK_PR_THEN simplecommandcall 
                                        {              
 							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_IF_ELSE;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_ternary_node((void*)value,$3, $6, NULL);    
                                        }
               |TK_PR_IF '('expression')'TK_PR_THEN simplecommandcall TK_PR_ELSE simplecommandcall
                                        {              
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_IF_ELSE;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_ternary_node((void*)value,$3, $6, $8);    
                                        };

//-----------------------------------------------------------------------------------------
// WHILE
whiledec:       TK_PR_WHILE '('expression')' TK_PR_DO simplecommandcall{               
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_WHILE_DO;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$6,$3);    

                                 }  ;


//-----------------------------------------------------------------------------------------
//DOWHILE
dowhiledec:     TK_PR_DO simplecommandcall TK_PR_WHILE '('expression')' {              
							node_value *value = malloc(sizeof(node_value));
							value->node_type = AST_DO_WHILE;
							value->SymbolTableEntry = NULL; 					
							$$ = tree_make_binary_node((void*)value,$2, $5);    
                                        };
//-----------------------------------------------------------------------------------------

/*//SWITCH - o comando pode ser qualquer um ou só case?
switchdec:      TK_PR_SWITCH '('expression')' simplecommandcall ;
//-----------------------------------------------------------------------------------------
// FOREACH
foreachdec:     TK_PR_FOREACH '('TK_IDENTIFICADOR':' expression foreachList')' simplecommandcall ;

foreachList:    ','expression
		|//nothing
 		;
//-----------------------------------------------------------------------------------------
// FOR
fordec:         TK_PR_FOR '('simplecommandcall forcommand':' expression':' simplecommandcall forcommand')' simplecommandcall ;

forcommand:     ','simplecommandcall
                |//nothing
 		;
*/
%%
